//Generated by wsdl2apex

public class soapSforceComSchemasClassItem {
    public class ITEM {
        public String obj_name;
        public String obj_value;
        private String[] obj_name_type_info = new String[]{'obj_name','http://soap.sforce.com/schemas/class/ITEM',null,'0','1','true'};
        private String[] obj_value_type_info = new String[]{'obj_value','http://soap.sforce.com/schemas/class/ITEM',null,'0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/schemas/class/ITEM','true','false'};
        private String[] field_order_type_info = new String[]{'obj_name','obj_value'};
    }
}