@isTest
Private class TestSendEmail
{
    static testmethod void test() 
    {
        // The query used by the batch job.
        
        List<Case> lstCases =[SELECT Status,Type FROM Case Where Status  = 'New' And Type = 'Other'];
        Case[] c1 = new List<Case>();
        
            case c = new Case();
            c.Status = 'Closed';
            c.Type = 'Electrical';
            c1.add(c);
            insert c1;
            system.debug('value of c1 on insert '+c1);
            
            c.Status = 'New';
            c.Type= 'Other';
            update c;
            c1.add(c);
            system.debug('value of c1 on update'+c1);
       
        
       Test.StartTest();
       SendEmail sd = new SendEmail();
       ID batchprocessid = Database.executeBatch(sd);
       Test.StopTest();
       System.assertEquals(lstCases,c1);
    }
}