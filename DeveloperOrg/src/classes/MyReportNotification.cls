public with sharing class MyReportNotification implements Reports.NotificationAction {
    
    public void execute(Reports.NotificationActionContext context){
    	Reports.ReportResults results = context.getReportInstance().getReportResults();
    	System.debug(context.getThresholdInformation().getEvaluatedConditions()[0].getValue());
    	System.debug(results.getReportMetadata().getName());
    }
}