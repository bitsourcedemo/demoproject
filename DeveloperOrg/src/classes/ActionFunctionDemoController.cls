/*  PASSING VALUES TO JAVASCRIPT FROM APEX CONTROLLER METHOD reciepe
    page name:demoofjs

*/


public with sharing class ActionFunctionDemoController {

      public Account acc {get;set;}
      public String message {get;set;}

         public ActionFunctionDemoController()
         {
            acc = new Account(name='Some Value',sam__c='0019000001GwuJv');
            message = 'Try again!!'; // initial message.
         }
        public PageReference save() { 
        
        try {
              insert acc;
              message = 'Insert successful! -- ' + acc.id;
            
            } 
        catch (Exception e) 
            {
              ApexPages.addMessages(e);
              message = 'Whoops! An error occurred -- ' + e.getMessage();      
            }
              return null;
  }
        





}