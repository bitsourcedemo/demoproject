// when account name  insert or update then the associated contact name will be updated by account name.
public class accountNameChangeTriggerOperations{

public static void sampleMethod(){
    List<Account> lstaccount = trigger.new;
    List<Contact> lstConToUpdate = New List<Contact>();
    Map<Id,Account> mapOfIds = new Map<Id,Account>();
    
    for(Account acctemp :lstaccount){
        mapOfIds.put(acctemp.Id, acctemp);
    }
    List<Contact> lstContact = [Select FirstName, 
                                       LastName, 
                                       Account.Name, 
                                       Account.Industry 
                                       from contact where Account.id IN : mapOfIds.Keyset()];
    for(Account acctempvar :lstaccount){
        for(Contact conTemp :lstContact){
            if(acctempvar.Name != null){
                conTemp.FirstName = acctempvar.Name;
                lstConToUpdate.add(conTemp);
            }
        }
    }
        database.upsert(lstConToUpdate);
    }
    
}