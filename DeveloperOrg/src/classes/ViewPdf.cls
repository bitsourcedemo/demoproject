public class ViewPdf {
    public Attachment att {
        get {
            if (att == null) {
                String pdfContent = 'CE993A13754340D1FED14A9EFB4A058D';
                Account a = new account(name = 'test');
                insert a;
                Attachment attachmentPDF = new Attachment();
                system.debug('value of the parent Id :  '+attachmentPdf.parentId);
                attachmentPdf.parentId = a.id;
                attachmentPdf.name = account.name + '.pdf';
                attachmentPdf.body = blob.toPDF(pdfContent);
                system.debug('--attachmentPdf.body--'+attachmentPdf.body);
                insert attachmentPDF;
                system.debug('--attachmentPDF--'+attachmentPDF);
                system.debug('--att--'+att);
                att = attachmentPDF;
            }
            return att;
        }
        private set;
    }
    public String pdf {
        get {
        //system.debug('--EncodingUtil.Base64Encode(att.body)--'+EncodingUtil.Base64Decode(att.body));
            return EncodingUtil.Base64Encode(att.body);
        }
    }
}