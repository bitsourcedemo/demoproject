public class ContactSearch
{
    public static List<Contact> searchForContacts(String Lstname, String PstlCode)
    {
        List<Contact> lstContact = new List<Contact>([Select Id,Name From Contact Where LastName =: Lstname AND MailingPostalCode =: PstlCode]);
         system.debug('list inside the method'+lstContact);
        return lstContact;          
    }
}