@isTest
private class accountTestTriggerHandler_Test
{    
    static testmethod void checkUpdate()
    {
        Account a = new Account();
        a.Name='test1';
        insert a;
        
        Contact c = new Contact();
        c.FirstName = 'Test';
        c.LastName = 'Data1';
        c.Salutation = 'Mr.';
        //c.Description = c.salutation+c.FirstName+c.LastName;
        insert c;
        system.debug('value of c after insert'+c);
        
        a.Name = 'Test2';
        update a;
        
        
        c.Description = c.salutation+c.FirstName+c.LastName;
        update c;
        system.debug('value of c after update'+c);
        
        String CheckString = c.Description;
        system.debug('value of c in checkString'+CheckString);
        //String sample=[SELECT Description FROM Contact WHERE Id = :c.Id].Description;
        //system.debug('value of description fromt the contact with the cid '+sample);
        //system.assert(c.Description,sample);
        system.assertEquals(CheckString, [SELECT Description FROM Contact WHERE Id = :c.Id].Description);   
    }

}