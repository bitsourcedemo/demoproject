@isTest
Private class AccountOwnerReassignmenttest { 

 public static testmethod void testBatchAccountOwnerReassignment()
 {
     // Access the standard user profile&nbsp; 
    
        Profile p = [SELECT Id FROM profile 
                     WHERE name='Standard User']; 

    // Create the two users for the test&nbsp; 
    
        User fromUser =new User(alias ='newUser1', 
            email='newuser1@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', 
            username='newuser1@testorg.com');
        User toUser =new User(alias ='newUser2', 
            email='newuser2@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', 
            username='newuser2@testorg.com');
        insert fromUser;
        insert toUser;
        
        // Use the new users to create a new account 
    
        List<Account> accs =new List<Account>();
        for(integer i = 0; i < 200; i++){
            accs.add(new Account(name ='test', 
                     ownerId = fromUser.id));
        }
        insert accs;

        // Actually start the test&nbsp; 
    
        Test.startTest();
        Database.executeBatch(new 
                 AccountOwnerReassignment(fromUser, 
                              toUser));
        Test.stopTest();

        // Verify the test worked&nbsp; 
    
        accs = [SELECT id, name FROM account 
                WHERE ownerId = :toUser.id];
        System.assert(accs.size() == 200);
    }
}