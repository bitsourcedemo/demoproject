//web services example with the util classes.
// @author : Patrick 

global with sharing class AccountAPI {
    /**
    * Gets an account and all of the related contacts
    * 
    * @param aContext The account to get
    */
    WebService static APIUtils.APIAccount getAccount(APIUtils.AccountContext aContext) {
        APIUtils.APIAccount result = new APIUtils.APIAccount();

        try {
            APIUtils.ensureContext(aContext);

            result = new APIUtils.APIAccount(aContext.getAccount());
            result.addContacts(aContext.getContacts());
        } catch (GenericUtils.BadException e) {
            result.returnCode = APIUtils.STATUS_BAD;
            result.message = e.getMessage();
        } catch (GenericUtils.UnknownException e) {
            result.returnCode = APIUtils.STATUS_NOTFOUND;
            result.message = e.getMessage();
        } catch (Exception e) {
            result.returnCode = APIUtils.STATUS_ISE;
            result.message = e.getMessage();
        }

        return result;
    }
}