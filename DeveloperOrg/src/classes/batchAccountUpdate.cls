global class batchAccountUpdate implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,Name FROM Account';
        return Database.getQueryLocator(query);
    }
    global void Execute(Database.BatchableContext BC, List<Account> scope){
        for(Account a : scope){
            a.Name = a.Name+'updated'+'latest';
        }
        update scope;
    }
    global void Finish(Database.BatchableContext BC){
        
    }
}