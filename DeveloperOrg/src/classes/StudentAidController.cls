public class StudentAidController {
	
    private final Student__c studentRecord;
    
    public StudentAidController(Apexpages.StandardController sc)
    {
        studentRecord = (Student__c)sc.getRecord();
    }
    
    public PageReference applyForAid()
    {
        studentRecord.AppliedForFinancialAid__c = true;
        update studentRecord;
        return null;
    }
	
    @AuraEnabled
    public static Student__c getStudent(ID studentId)
    {
        if(studentId ==  null){
            List<Student__c> students = [Select ID,AppliedForFinancialAid__c From  Student__c where Id = :studentId];
            if(students.size() >0){
                return students[0];
            }
        }
        return null;
    }
	
	@RemoteAction @AuraEnabled
    public static void applyForAidAction(ID studentId)
    {
		List<Student__c> students = [Select ID,AppliedForFinancialAid__c From  Student__c where Id = :studentId];
		 if(students.size() >0){
                students[0].AppliedForFinancialAid__c = true;
             	update students;
            }        
    }    
}