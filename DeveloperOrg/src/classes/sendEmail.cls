global class sendEmail implements Database.Batchable<sObject> {
     
    global sendEmail()
    {}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id,Status,Subject,Type FROM Case]);   
    }

    global void execute(Database.BatchableContext BC, List<Case> cases) {
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+cases);
        for(Case c: cases )
        {
            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+c.Status);
            if(c.Status=='New')
            {
               c.Type ='Other';
            }
            update c;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(new String[] {'lalit.singh@accenture.com'});
      mail.setReplyTo('lalitsngh@gmail.com');
      mail.setSenderDisplayName('Batch Processing Team');
      mail.setSubject('Batch Process Completed');
      mail.setPlainTextBody('Batch Process has completed');
      String messageBody = '<html><body style="background-color:lightgrey">Hi, <br><br><h2 style="color:Green">Batch Class Has Been Succesfully Executed;</h2><br><br><img src="{!$Resource.spurs}" style="width:304px;height:228px"><iframe src="demo_iframe.htm" width="200" height="200"></iframe><br><br> <br>Batch Processing Team<br><br>Please Do Not Reply On This Mail As It Is System Generated Mail</body></html>';
      mail.setHtmlBody(messageBody); 
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}