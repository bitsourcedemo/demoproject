public with sharing class CustomPermissionClass { 
    
    public Boolean accessBool {get;set;}
    
    public CustomPermissionClass() {
        accessBool = checkUserHasCustomPermissionAccess('Create_Account', UserInfo.getUserId());
    }
    
    public static Boolean checkUserHasCustomPermissionAccess(String permissionName, Id userId) {
        Set <Id> permissionSetIds = new Set <Id>();
        List<User> userList = new List <User>();
        for ( SetupEntityAccess access : [ SELECT ParentId FROM SetupEntityAccess 
                                           WHERE SetupEntityId IN ( SELECT Id 
                                                                    FROM CustomPermission 
                                                                    WHERE DeveloperName = :permissionName )]) 
            permissionSetIds.add(access.ParentId);
        userList = [ SELECT Username FROM User WHERE Id IN (
                SELECT AssigneeId FROM PermissionSetAssignment
                WHERE PermissionSetId IN :permissionSetIds AND AssigneeId =: userId)];
        return userList.isEmpty() ? false : true;
    }
    
}