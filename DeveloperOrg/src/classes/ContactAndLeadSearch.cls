public class ContactAndLeadSearch
{
    public Static List<List< SObject>> searchContactsAndLeads(String search)
    {
        List<List<Sobject>> lstdisplay; 
        List<List<SObject>> lstAll = [Find 'search' IN All Fields RETURNING Lead(FirstName,LastName),Contact(FirstName,LastName,Department)];
        system.debug('list of the contact and lead is : '+lstAll);
        
        lstdisplay.add(lstAll[0]);
        lstdisplay.add(lstAll[1]);
        return lstdisplay;
    }
}