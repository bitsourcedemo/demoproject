global class OwnerReassignment implements DataBase.Batchable<sObject>,Database.Stateful
{
    String query;
    String email;
    Id toUserId;
    Id fromUserId;
   
  global Database.querylocator start(Database.BatchableContext BC){
  
      return Database.getqueryLocator(query);
  }
  
  global void execute(Database.BatchableContext BC,List<sObject> scope)
  {
      List<Account> accns = new List<Account>();
      for(sobject s :scope){
      Account a = (Account)s;
      if(a.OwnerId==fromUserId){
          a.OwnerId=toUserId;
          accns.add(a);
         }
      }update accns;
    }
   global void finish(Database.BatchableContext BC)
   {
        Messaging.SingleEmailmessage mail =  new  Messaging.SingleEmailmessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('lalitsngh@gmail.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }
}