public with sharing class GetCaseIdController {
	public static void getCaseIdSample(){
		// Get email thread ID
		String emailThreadId = '_00Dxx1gEW._500xxYktg';
		// Call Apex method to retrieve case ID from email thread ID
		ID caseId = Cases.getCaseIdFromEmailThreadId(emailThreadId);
	}

}