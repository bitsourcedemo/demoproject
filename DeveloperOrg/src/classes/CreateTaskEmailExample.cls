global class CreateTaskEmailExample implements Messaging.InboundEmailHandler
{
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,Messaging.InboundEnvelope env)
    {
        //creating an inbound email result object for returnning the result of the apex email service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String myPlainText= '';
        
        // Add the email plain text into the local variable
        myPlainText = email.plainTextBody;
        
        //new task object to create
        Task[] newTask = new Task[0];
        
        try
        {
            Contact vCon = [Select Id,Name,Email From Contact WHERE Email = : email.fromAddress LIMIT 1];
            
            //add a new task to the contact record we just found above
            newTask.add(new Task(Description = myPlainText,
                                  Priority = 'Normal',
                                  Status='Inbound Email',
                                  Subject = email.subject,
                                  IsReminderSet = true,
                                  ReminderDateTime =System.now()+1,
                                  whoId = vCon.Id
                                  ));
           // insert the new task
           insert newTask;
           
           System.debug('New Task Object:'+newTask);
        }
        catch(QueryException e)
        {
            system.debug('Query issue:'+e);
        }
        result.Success = true;
        
        return result;
        }
}