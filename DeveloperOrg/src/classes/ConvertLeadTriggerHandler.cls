public with sharing class ConvertLeadTriggerHandler
{
     public void contlead(List<Lead> lstLead)
     {
         Set<Id> setId = new Set<Id>();
         for(Lead obj:lstLead)
         {
             setId.add(obj.Id);
         }
        LeadStatus convertStatus = [select MasterLabel from LeadStatus where IsConverted = true limit 1];
        List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();
        
        List<Lead> lstLeads =([Select Id,Name,IsConverted,LeadSource,Status From Lead where Id =: setId]);
        for (Lead lead:lstLeads) 
        {
            if (!lead.isConverted && lead.LeadSource =='Partner Referral') 
            {
                 Database.LeadConvert lc = new Database.LeadConvert();
                 String oppName = lead.Name;
                 
                 lc.setLeadId(lead.Id);
                 lc.setOpportunityName(oppName);
                 lc.setConvertedStatus(convertStatus.MasterLabel);
                 
                 leadConverts.add(lc);
            }
        
        }
        if (!leadConverts.isEmpty())
        {
          List<Database.LeadConvertResult> lcr = Database.convertLead(leadConverts);
        }
    }    
}