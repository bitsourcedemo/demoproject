public class DuplicateContactCheck{
    
    public void methodToCheck()
    {
        Contact Jane = new Contact (FirstName='Jane',LastName='Smith',Email='jane.smith@example.com',Description='Contact of the day');
        
        insert jane;
        
        Contact jane2 = new Contact(FirstName='Jane',LastName='Smith',Email='jane.smith@example.com',Description='Prefers to be contacted by email.');
        
        upsert jane2 Contact.fields.Email;
        
        System.assertEquals('Prefers to be contacted by email.',[SELECT Description FROM Contact WHERE Id=:jane.Id].Description);
    }
}