/*******************************************************************
 * Custom controller for the "Set Value into Controller Property" 
 * recipe. 
 * Retrieves a list of accounts of a particular type 
 *******************************************************************/
 public with sharing class AccountsTypeController {
        //account type to retreive
        public String accType{get;set;}

        //maximum number of accounts to retreive
        public Integer max {get;set;}

        //retreives the accounts
        public list<Account> getAccounts(){
                return [Select Id,Name,phone,BillingState,Type,Industry,AnnualRevenue From Account Where Type =:accType Limit:max ];
        }
        
}