public with sharing class PocRecordTypetriggerHandler {
    
    
    
    public static Map<Id,Recordtype> returnRecordtypeValur(set<String> recordtypenames){
        Map<Id,RecordType> rt = new Map<Id,RecordType>([SELECT Id FROM RecordType WHERE DeveloperName in :recordtypenames]);
        system.debug(rt);
        return rt;
    }
    
   public static String OtherRecordTypeId {
    get{
      if(OtherRecordTypeId == null){
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Other'];
        OtherRecordTypeId = rt.Id;
      }
      system.debug('OtherRecordTypeId  :   '+OtherRecordTypeId);
      return OtherRecordTypeId;
    }
  } 
  
   public static String TestOtherRecordTypeId {
    get{
      if(TestOtherRecordTypeId == null){
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'TestOther'];
        TestOtherRecordTypeId = rt.Id;
      }
      system.debug('TestOtherRecordTypeId  :   '+TestOtherRecordTypeId);
      return TestOtherRecordTypeId;
    }
  } 
  
  public static String HRTicketRecordTypeId {
    get{
      if(HRTicketRecordTypeId == null){
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'sample'];
        HRTicketRecordTypeId = rt.Id;
      }
      system.debug('HRTicketRecordTypeId  :   '+HRTicketRecordTypeId);
      return HRTicketRecordTypeId;
    }
  } 
}