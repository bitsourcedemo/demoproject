public with sharing class DynamicAccountFieldsLister {
	
	public DynamicAccountFieldsLister(ApexPages.StandardController controller){
		controller.addFields(editableFields);
		system.debug('--Controller value --  : '+controller);
	}
	
	public List<String> editableFields{
		get{
			if(editableFields == null){
				editableFields = new List<String>();
				editableFields.add('Industry');
				editableFields.add('AnnualRevenue');
				editableFields.add('BillingCity');
			}
			system.debug('--editableFields --  : '+editableFields);
			return editableFields;
		}
		private set;
	}
}