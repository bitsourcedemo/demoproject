//How to add from PageBlockTable to another PageBlockTable in Visualforce page fucntionality
//page name:
public class AddToTable {

    public List<Account> listAccount {get;set;}    
    public List<Account> listAccountAdded {get;set;}
    public Integer rowNum {get;set;}    
    public Integer rowNum1 {get;set;}
    
    public AddToTable(){
         listAccount = [SELECT Name, Industry FROM Account];
         listAccountAdded = new List<Account>();
    }
    public void addToList() {
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        listAccountAdded.add(listAccount.get(rowNum));
        listAccount.remove(rowNum);
    }
    public void removeFromList()
    {
        rowNum1 = Integer.valueOf(apexpages.currentpage().getparameters().get('index1'));
        listAccount.add(listAccountAdded.get(rowNum1));
        listAccountAdded.remove(rowNum1);
    }
}