public with sharing class AccountDataController
{

    public AccountDataController(ApexPages.StandardController controller) {

    }
        public PageReference savePage()
        {
            Pagereference Savepage = new Pagereference ('/apex/AccountDemo');
            Savepage.setRedirect(true);
            return Savepage;
        }
        
        public PageReference nextPage(){
            Pagereference nextpage = new Pagereference ('/apex/AccountDemo2');
            nextpage.setRedirect(true);
            return nextpage;
        }
        
        public PageReference Previous()
        {
        
            Pagereference Previouspage = new Pagereference ('/apex/AccountDemo');
            Previouspage.setRedirect(true);
            return Previouspage;
        
        }
}