public class PositionC_Utilities{
    public static void assignRegion1(Position__c position)
    {
        User recruiter = [Select Region__c From User WHERE Id =:position.OwnerId];
        position.Region__c=recruiter.Region__c;
    }

}