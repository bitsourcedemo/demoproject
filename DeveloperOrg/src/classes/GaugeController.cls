public class GaugeController {
    private static Integer LOWER = 4;
    private static Integer MID = 8;

    public String gaugeColor {
        get;
        set;
    }

    public class GaugeData {
        public String name {
            get;
            set;
        }

        public Integer size {
            get;
            set;
        }

        public GaugeData(String name, Integer size) {
            this.name = name;
            this.size = size;
        }
    }

    public List<GaugeData> data {
        get;
        set;
    }

    public GaugeController() {
        this.data = new List<GaugeData>();

        List<Report> reportList = [
            select Id
            from Report
            where DeveloperName = 'lalit singh'
        ];

        Reports.ReportResults results = Reports.ReportManager.runReport((String) reportList.get(0).Id, true);
        Reports.Dimension dim = results.getGroupingsDown();

        Integer numberOfCases = 0;

        for (Reports.GroupingValue groupingVal : dim.getGroupings()) {
            String factMapKey = groupingVal.getKey() + '!T';
            Reports.ReportFactWithDetails factDetails = (Reports.ReportFactWithDetails)results.getFactMap().get(factMapKey);
            Reports.SummaryValue sumVal = factDetails.getAggregates()[0];
            numberOfCases += Integer.valueOf(sumVal.getLabel());
        }

        this.data.add(new GaugeData('Cases', numberOfCases));

        if (numberOfCases < LOWER) {
            this.gaugeColor = '#093, #ddd';
        } else if (numberOfCases < MID) {
            this.gaugeColor = '#ff6, #ddd';
        } else {
            this.gaugeColor = '#c00, #ddd';
        }
    }
}