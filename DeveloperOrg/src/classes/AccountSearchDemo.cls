public with sharing class AccountSearchDemo
{
     public Boolean searched {get; set;}
     public List<Account> AccRec{get;set;}
     public List<Contact> ContactList{get;set;}
     public String selectedId;
     public Account acc{get;set;}
     public String inputName{get;set;}
     public String phone{get;set;}
     public String rating{get;set;}
     public String industry{get;set;}
     public Boolean Match{get;set;}
     public Boolean NoMatch{get;set;}
     public Boolean AllMatch{get;set;}
     public Boolean Buttn{get;set;}
     public Boolean AllContact{get;set;}
     public Boolean recNoConMatch{get;set;}
     public integer totalrecs =0;
     public AccountSearchDemo(ApexPages.StandardController controller)
     {
         selectedId ='';
         acc= new Account();
         totalrecs = [Select count() from Account];
     }
     
     public void AccountRecords(){
         Match=false;
         NoMatch=false;
         AllMatch = false;
         Buttn = false;
         Allcontact=false;
         ContactList = new List<Contact>();
         
         
         if((inputName != null && inputName!='') || (phone != null && phone!='') || (rating != null && rating != '') || (industry != null && industry != '')){
             String query = 'Select Name,Rating,Phone,Industry from Account Where ';
             if(inputName!= null && inputName!=''){
                query +=' ( Name=\''+ inputName+'\' ) ';
            }
            if(phone!= null && phone!=''){
               if(inputName== null || inputName==''){
                    query +=' ( phone=\''+ phone+'\' ) ';
                }
                else{
                    query +='And (phone=\''+phone+'\')';
                }
            }
            
            if(rating!= null && rating !=''){
                if((inputName==null || inputName=='') && (phone==null || phone==''))
                {
                    query +=' ( rating=\''+ rating+'\' ) ';
                }
                else
                {
                    query +='And (rating=\''+ rating+'\')';
                }
            }
            
            if(industry != null && industry !=''){
                if((inputName== null || inputName=='') && (phone== null || phone=='') && (rating == null || rating ==''))
                {
                    query +=' ( industry=\''+ industry+'\' ) ';
                }
                else
                {
                    query +='And (industry=\''+industry+'\')';
                }
                
            }
                AccRec = Database.query(query);
                if(AccRec.Size()>0){
                        Match = True;
                    }
                    else
                    {  
                        NoMatch = True;
                    }

         }
         else
         {
            AllMatch = True;
            Buttn = True;
            AccRec =  [Select Name,Rating,Phone,Industry from Account];
         }
     }

     public pageReference selectedTaskId(){

        selectedId = ApexPages.currentPage().getParameters().get('selectedId');
        contactdata();
        return null;
     }

     public void contactdata(){
        if(selectedId !=''){
            ContactList = [Select Id,Name,account.Id,Account.Name from Contact Where account.Id =: selectedId limit 10];
            if(!ContactList.isEmpty()){
                AllContact =  True;
                recNoConMatch=false;
        }else{
            AllContact = false;
            recNoConMatch= True;
        }
        }
     }
}