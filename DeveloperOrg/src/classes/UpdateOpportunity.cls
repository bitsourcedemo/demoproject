global class UpdateOpportunity implements Database.Batchable<sObject>{

    string query;

    global Database.querylocator start(Database.BatchableContext BC){
        Query = 'Select id, name,Stage__c From Opportunity';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> scope1){
       system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+scope1);

        for(Opportunity p : Scope1){
            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+p.stage__c);    
              if(p.stage__c == 'Closed Won')
              {
               p.Name = 'test';
              }    
               update p;  
        }       
    }

    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(new String[] {'lalitsngh@gmail.com','lalit.singh@accenture.com'});
      mail.setReplyTo('lalitsngh@gmail.com');
      mail.setSenderDisplayName('Batch Processing');
      mail.setSubject('Batch Process Completed');
      mail.setPlainTextBody('Batch Process has completed');
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}