public class UpdateValues implements Database.Batchable<sObject> {
    
 public String queryString='';
 
 public final string typevalue = 'Customer - Direct';
 
 public Database.QueryLocator start(Database.BatchableContext bc) {
     
     queryString = 'select Id,Name,SLA__c  From Account where Type =: typevalue';
     return Database.getQueryLocator(queryString);
 }

 public void execute(Database.BatchableContext bc, List<Account>  accountLst){ 
        
        try{
        
        List<Account> updateAccountList = new List<Account>();
            for(Account accTemp : accountLst){
                accTemp.SLA__c = 'Gold';
                updateAccountList.add(accTemp);
            }
                database.update(updateAccountList);
           }
        catch(exception ex){
         System.debug('Following exception occurred :'+ex.getMessage());
        }
 }
 
 public void finish(Database.BatchableContext bc) {
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(new String[] {'lalit.singh@kpit.com'});
      mail.setReplyTo('lalit.singh@kpit.com');
      mail.setSenderDisplayName('Batch Processing Team');
      mail.setSubject('Batch Process Completed');
      mail.setPlainTextBody('Batch Process has completed');
      String messageBody = '<html><body style="background-color:lightgrey">Hi, <br><br><h2 style="color:Green">Batch Class Has Been Succesfully Executed;</h2><br><br><img src="{!$Resource.spurs}" style="width:304px;height:228px"><iframe src="demo_iframe.htm" width="200" height="200"></iframe><br><br> <br>Batch Processing Team<br><br>Please Do Not Reply On This Mail As It Is System Generated Mail</body></html>';
      mail.setHtmlBody(messageBody); 
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 }
}