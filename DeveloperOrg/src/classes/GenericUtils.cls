//web services example with the util classes.
// @author : Patrick 
public class GenericUtils {
    
    public Virtual class BadException extends Exception{}
    public virtual class UnknownException extends Exception {}
    
    /**
    * Checks to see if a string is null or blank
    * @param value The string to check
    * @return If the string is blank
    */
    public static Boolean isBlank(String value){
        if(value== null){
            return true;
        }
        return (value.Trim().Length() == 0);
    }
}