@isTest
private class ConvertLeadTriggerHandler_Test
{
    private static Integer LEAD_COUNT = 0;
    
    private static Lead CreateLead()
    {
        LEAD_COUNT += 1;
        return new Lead( FirstName = '_unittest_firstname_: ' + LEAD_COUNT,
            LastName = '_unittest_lastname_: ' + LEAD_COUNT,
            Company = '_unittest_company_: ' + LEAD_COUNT,
            Status = 'Inquiry',
            LeadSource = 'Partner Referral');
    }
    
     public static void PartnerReferral(Lead lead) {
        lead.LeadSource = 'Partner Referral';
    }
    
    public static void web(Lead lead){
        lead.LeadSource = 'Web';
    }
    public static List<Lead> fetchLeads(Set<Id> ids)
    {
        return 
        [
            select isConverted from Lead where Id in :ids
        ];
    }
    
    public static testMethod void trialConvert() {
        Lead testLead = createLead();
        PartnerReferral(testLead);
 
        Test.startTest();
        
        insert testLead;
        
        Test.stopTest();
        
        List<Lead> results = fetchLeads(new Set<Id>{testLead.Id});
        
        System.assertEquals(1, results.size(), 'Did not get the right number of leads back');
        System.assert(results.get(0).isConverted, 'The lead should have been converted since it was a "partner Referral"');
    }
    
    public static testMethod void nonTrialNoConvert()
    {
        Lead testLead = createLead();
        Web(testLead);
        
        Test.startTest();
        
        insert testLead;
        
        Test.stopTest();
        
        List<Lead> results = fetchLeads(new Set<Id>{testLead.Id});
        
        System.assertEquals(1, results.size(), 'Did not get the right number of leads back');
        System.assert(!results.get(0).isConverted, 'The lead should not have been converted since leadSource is web');
    }
    
    public static testmethod void bulkTest() {
    
    List<Lead> shouldBeConverted = new List<Lead>();
    List<Lead> shouldNotBeConverted = new List<Lead>();
    
    for (Integer i = 0; i < 50; i++)
    {
            Lead testLeadNonConvert = createLead();
            web(testLeadNonConvert);
            Lead testLeadConvert = createLead();
            PartnerReferral(testLeadConvert);
            
            shouldBeConverted.add(testLeadConvert);
            shouldNotBeConverted.add(testLeadNonConvert);
        }
        List<Lead> toInsert = new List<Lead>();
        toInsert.addAll(shouldBeConverted);
        toInsert.addAll(shouldNotBeConverted);
        Test.startTest();
        
        insert toInsert;
        
        Test.stopTest();
        
        Map<Id, Lead> expectedConversions = new Map<Id, Lead>(shouldBeConverted);
        Map<Id, Lead> expectedNonConversions = new Map<Id, Lead>(shouldNotBeConverted);
        
        Set<Id> leadIds = new Set<Id>();
        leadIds.addAll(expectedConversions.keySet());
        leadIds.addAll(expectedNonConversions.keySet());
        for (Lead result: fetchLeads(leadIds)) {
            if (expectedConversions.containsKey(result.Id)) {
                System.assert(result.isConverted, 'This lead should have been converted ' + result);
                expectedConversions.remove(result.Id);
            } else if (expectedNonConversions.containsKey(result.Id)) {
                System.assert(!result.isConverted, 'This lead should not have been converted ' + result);
                expectedNonConversions.remove(result.Id);
            } else {
                System.assert(false, 'We got a Lead we did not expect to get back ' + result);
            }
        }
        
        System.assert(expectedConversions.isEmpty(), 'We did not get back all the converted leads we expected');
        System.assert(expectedNonConversions.isEmpty(), 'We did not get back all the non converted leads we expected');
    }
    
}