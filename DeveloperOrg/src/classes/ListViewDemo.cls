/**
*  Description : Controller Class to show How to utilize existing List View in Apex with Pagination Support
*
*  Author : lalit singh
*/
public with sharing class ListViewDemo {
    
  public String baseQuery = 'Select Id,Name,Industry,NumberOfEmployees,Phone,Rating,Type,AnnualRevenue FROM Account ';
  
  public String AccFilterId {get; set;}
    
  private Integer pageSize = 10;
  
  public ApexPages.StandardSetController AccSetController;
  
  private Set<Id> selectedAccIds;
    
  public ListViewDemo(){
      //init variable
        this.selectedAccIds= new Set<Id>();
        
        if(this.AccSetController == null){
                this.AccSetController = new ApexPages.StandardSetController(Database.getQueryLocator(baseQuery));
                this.AccSetController.setPageSize(pageSize);
                this.AccSetController.setpageNumber(1);   
  }
  }
  
    public void doSelectItem(){
        this.selectedAccIds.add(this.AccFilterId);
    }
    //Handle Item Deselected.
    public void doDeselectItem(){
        this.selectedAccIds.remove(this.AccFilterId);
    }
    //returning the count of the selected items
    public Integer getSelectedCount(){
        System.debug('value of the getseleceted item----------------------------------'+selectedAccIds.size());
        return this.selectedAccIds.size();
        
    }
    //Navigate to Next page
    public void next()
    {
      if(this.AccSetController.getHasNext())
      {
        this.AccSetController.next();
      }
    }
    //Navigate to Prev Page
    public void prev()
    {
      if(this.AccSetController.getHasPrevious())
      {
        this.AccSetController.previous();
      }
    }
    /*
    *   return current page of groups
    */
    public List<RowItem> getAccounts()
    {
      //return (List<Account>)AccSetController.getRecords();
      
      List<RowItem> rows = new List<RowItem>();
 
        for(sObject r : this.AccSetController.getRecords()){
            Account c = (Account)r;
 
            RowItem row = new RowItem(c,false);
            if(this.selectedAccIds.contains(c.Id)){
                row.IsSelected=true;
            }
            else{
                row.IsSelected=false;
            }
            rows.add(row);
        }
 
        return rows;
    }
    /*
    *   return whether previous page exists
    */
    public Boolean getHasPrevious(){
 
        return this.AccSetController.getHasPrevious();
 }
  /*
    *   return whether next page exists
    */
    public Boolean getHasNext(){
 
        return this.AccSetController.getHasNext();
 
    }
    //return page number
    public Integer getPageNumber(){
        return this.AccSetController.getPageNumber();
    }
     //return total pages
    public Integer getTotalPages(){
        Decimal totalSize = this.AccSetController.getResultSize();
        Decimal pageSize = this.AccSetController.getPageSize();
        Decimal pages = totalSize/pageSize;
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    
    public with sharing class RowItem{
             
            public Account tAccount{get;set;}
            public Boolean IsSelected{get;set;}
            
            public RowItem(Account a , Boolean b)
            {
            this.tAccount = a;
            this.IsSelected = b;
        }
    }
    public ListViewDemo(ApexPages.StandardSetController c) {   }
 
    //Navigate to first Page
    public void firstPage()
    {
      this.AccSetController.first();
    }
 
    //Navigate to last Page
    public void lastPage()
    {
      this.AccSetController.last();
    }
}