//javascript remoting example
global with sharing class AccountRemoter {
    
    public string accountName{get;set;}
    public Static Account account {get;set;}
    
    public AccountRemoter(){
        //Empty Constructor
    }
    
    @RemoteAction
    global Static Account getAccount(String accountName){
        account = [Select Type,Phone,NumberOfEmployees,Name From Account Where Name =:accountName];
        system.debug('value of account is in the remote action method'+account);
        return account;

    }
}