// read the multiple values from the url .
// in this receipe we will get the multiple values from the url in the parameter.


public class UriController {
/*    //*********************************************************************************
    // *Tip : Get the PageParameters in a Map and get key,values pairs smoothly of this 
    //*********************************************************************************
    
    //if the url parameters are multiple like this below parameter.
    // URi to read - http://test.salesforce.com/apex/helloworld?q=texas&city=Austin&zip=78666&ext=1234
    
    private string thequery {get; set;}
    private string thecity { get; set;}
    private string thezip { get; set;}
    private string theext { get; set;}
    
    //Map of PageParameters 
    private Map<string,string> pageParams=ApexPages.currentPage().getParameters();
    
    //Set the variable in Constructor
    public UriController() 
    {
      //get each variable through Map
     thequery = ApexPages.currentPage.getParameters.get('q');
     thecity = ApexPages.currentPage.getParameters.get('city');
     thezip = ApexPages.currentPage.getParameters.get('zip');
     theext = ApexPages.currentPage.getParameters.get('ext');
    }
    */


//with the method
 // URi to read - http://test.salesforce.com/apex/helloworld?q=texas&city=Austin&zip=78666&ext=1234
    
  /*  private string thequery {get; set;}
    private string thecity { get; set;}
    private string thezip { get; set;}
    private string theext { get; set;}
    
    //Map of PageParameters 
    private Map<string,string> pageParams=ApexPages.currentPage().getParameters();
    
    //Set the variable in Constructor
    public UriController() 
    {
      //get each variable through Map
     thequery = GetValueFromParam('q');
     thecity = GetValueFromParam('city');
     thezip = GetValueFromParam('zip');
     theext = GetValueFromParam('ext');
    }
    
    public static string GetValueFromParam(string ParamKey)
    {
        string toReturn='';
        if(ApexPages.currentPage().getParameters().containsKey(ParamKey))
        {            
            toReturn=ApexPages.currentPage().getParameters().get(ParamKey);
            toReturn=ApexUtil.GetCleanString(toReturn);        
        }
        return toReturn;
    }   */
    }