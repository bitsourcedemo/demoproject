public class CustomSearchController {
    public List<Account> lstAccounts{get;set;}
    public List<Contact> lstContacts {get;set;}
    public List<Opportunity> lstOppty {get;set;}
    public List<Lead> lstLeads {get;set;}
    public List<Position__C> lstPos {get;set;}
    public List<List<Sobject>> lstSobject {get;set;}
	public string searchKey {get;set;}
    
    public pagereference DoGlobalSearch(){
        String strSearchQuery = 'FIND' + '\\' + searchkey+ '*' + '\\' + 'IN ALL FIELDS RETURNING Account(id,name,rating,industry,type, annualrevenue),Contact(id, firstname,lastname,phone,email,fax),opportunity(id, name,stagename),lead(id,firstname,lastname,company,status),Position__C(id, name,location__C, open_date__c, close_date__C)';
        System.debug('——> ' +strSearchQuery);
		lstSobject = search.query(strSearchQuery);
		lstAccounts = new List<Account>();
		system.debug('———> ' + lstAccounts.size());
		lstContacts = new List<Contact>();
		lstOppty = new List<Opportunity>();
		lstLeads = new List<Lead>();
		lstPos = new List<Position__C>();
		lstAccounts = (List<Account>) lstSobject[0];
		lstContacts = (List<Contact>) lstSobject[1];
		lstOppty = (List<Opportunity>) lstSObject[2];
		lstLeads = (List<Lead>) lstSobject[3];
		lstPos = (List<Position__C>) lstSobject[4];
		return null;
    }
}