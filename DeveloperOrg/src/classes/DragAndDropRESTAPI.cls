@RestResource(urlMapping='/DragAndDrop/v1/*')
global with sharing class DragAndDropRESTAPI 
{
	@HttpPost
	global static String attachDoc(){
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		
		String fName = req.params.get('FileName');
		String parId = req.params.get('parId');
		Blob postcontent = req.RequestBody;
		
		Attachment a = new Attachment(ParentId = parId,Body = postcontent ,Name = fName);
		
		insert a;
		return a.Id;		
	}	
}