//Demo Rest Api..
@RestResource(urlMapping='/GetAccount/*')

// getting the values from the account object...

global with sharing class GetAccountValues {
    
    /*@HttpGet
    global Static List<Account> getAccounts()
    {
        List<Account> a = new List<Account>();
        a = [Select Id, Name,Phone From Account];
        return a;
    }*/
    
    @HttpGet
    global static Account doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String accountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account result = [SELECT Id, Name, Phone, Website FROM Account WHERE Id = :accountId];
        return result;
    }

    @HttpDelete
    global static void doDelete(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        String AccountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account account = [Select Id From Account WHERE Id =:AccountId];
        delete account;
    }
}