public with sharing class DateContainer {
	
	//date encapsulated
	public Date value{get;set;}

	//Constructor- takes the date to encapsulate
	public DateContainer( Date inVal) {
		value = inVal;
	}
}