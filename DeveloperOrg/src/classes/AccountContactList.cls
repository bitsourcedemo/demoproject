public class AccountContactList {
    public List<Account> getList(){
        return [Select Name, Id, (Select Name From Contacts) From Account];
    }
}