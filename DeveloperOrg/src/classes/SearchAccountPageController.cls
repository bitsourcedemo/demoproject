//original code for search

public class SearchAccountPageController{



    public Account Ac{get;set;}
    public List<Account> AccRec{get;set;} 
    public List<Contact> ContactList{get;set;} 
    public String selectedId ;
    public Boolean Match{get;set;}
    public Boolean NoMatch{get;set;}
    public Boolean AllMatch{get;set;}
    public Boolean AllContact{get;set;}
    public Boolean recNoConMatch{get;set;}
    public Boolean Buttn{get;set;}
    public String inputName{get;set;}
    public String type{get;set;}
    public integer totalRecs = 0;
    public integer OffsetSize = 0;
    public integer LimitSize= 10;
   // public String Categories{get;set;}
    //public List<SelectOption> getCategoriesOptions() {
    //List<SelectOption> CategoriesOptions = new List<SelectOption>();
    //CategoriesOptions.add(new SelectOption('','-None-'));
    //CategoriesOptions.add(new SelectOption('abc','ABC'));
    //CategoriesOptions.add(new SelectOption('efg','EFG'));
    //CategoriesOptions.add(new SelectOption('hij','HIJ'));
    //CategoriesOptions.add(new SelectOption('klm','KLM'));

    //return CategoriesOptions;
    //}
    
   public SearchAccountPageController(ApexPages.StandardController controller) {
       selectedId ='';
        Ac=new Account();
        totalRecs = [select count() from account];
    }
   public SearchAccountPageController(){
        //selectedId ='';
        //Ac=new Account();
    }
    public void createTable(){
        ContactList = new List<Contact>();
        Buttn = false;
        Allcontact=false;
        Match=false;
        NoMatch=false;
        AllMatch=false;    //|| (Categories!= null && Categories!='')
        if((inputName != null &&inputName !='') || (type != null && type != '') ){  
           string query = 'select AccountNumber,Name,Type from Account where  ';  //  ,Categories__c    
            if(inputName!= null && inputName!=''){
                query +=' ( Name=\''+ inputName+'\' ) '; 
            }  
            if(type!= null && type!='' ){
                if(inputName== null || inputName==''){
                    query +='( type=\''+ type+'\' ) '; 
                }
                else{
                    query +='And ( type=\''+ type+'\' ) '; 
                }
            } 
           /* if(Categories!= null && Categories!=''  ){
                if((inputName== null || inputName=='') && (type== null || type==''))
                {
                    query +=' ( Categories__c =\''+ Categories+'\' ) ';

                }else{
                    query +='And ( Categories__c =\''+ Categories+'\' ) '; 
                }
            }*/    
            AccRec=Database.query(query); 
            if(AccRec.size()>0)
            {
                Match=true;
            }else
            {
                NoMatch=true;
            }
        }
            else{
                AllMatch=true;
                Buttn =true;
                AccRec=[select AccountNumber,Name,Type from Account LIMIT :LimitSize OFFSET :OffsetSize];
            }

    }
    public pagereference selectTaskId(){    
        
        selectedId = ApexPAges.currentPage().getParameters().get('selectedId');
        contactData();
        return null;
    }   
    
    public void contactData(){
        
        if(selectedId != ''){
            ContactList= [Select Id,Name,account.Id ,Account.Name from Contact where account.Id =:selectedId limit 10];
            if(!ContactList.isEmpty()){
                AllContact=true;
                recNoConMatch=false;
                system.debug('@@@@ContactList'+ContactList[0].Name);
            } else{
            system.debug('I am inside Else');
            AllContact=false;
            recNoConMatch=true;
            }
        }


    }
    public void FirstPage(){
        OffsetSize = 0;
    }
    public void previous(){
        OffsetSize = (OffsetSize - LimitSize);
        AccRec=[select AccountNumber,Name,Type from Account LIMIT :LimitSize OFFSET :OffsetSize];
    }
    public void next(){
     OffsetSize = (OffsetSize + LimitSize); 
     AccRec=[select AccountNumber,Name,Type from Account LIMIT :LimitSize OFFSET :OffsetSize];
    }
    public void LastPage(){
        OffsetSize = (totalrecs - math.mod(totalRecs,LimitSize));
        AccRec=[select AccountNumber,Name,Type from Account LIMIT :LimitSize OFFSET :OffsetSize];
    }
    public boolean getprev(){
        if(OffsetSize == 0){
        AccRec=[select AccountNumber,Name,Type from Account LIMIT :LimitSize OFFSET :OffsetSize];
        return true;
        }else{
        return false;
        }
    }
    public boolean getnxt(){
        if((OffsetSize + LimitSize) > totalRecs){
        AccRec=[select AccountNumber,Name,Type from Account LIMIT :LimitSize OFFSET :OffsetSize];
        return true;
        
        }else{
        return false;
        }
    }

}