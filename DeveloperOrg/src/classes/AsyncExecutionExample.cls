public class AsyncExecutionExample implements Queueable {
    public void execute(QueueableContext context) {
        Account a = new Account(Name='Acme',Phone='(415) 555-1212', Multi_Picklist__c = 'Lohaghat');
        insert a;        
    }
}