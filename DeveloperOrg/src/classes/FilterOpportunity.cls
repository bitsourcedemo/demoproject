public class FilterOpportunity
{
    public String stage{get;set;}
    public list<opportunity> opp{get;set;}
    
    public List<SelectOption> getStages()
    { 
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('QF','Qualification'));
        options.add(new SelectOption('CW','Closed Won'));
        options.add(new SelectOption('CL','Closed Lost'));
        options.add(new SelectOption('N/R','Negotiation/Review')); 
        return options; 
    }
    
    public pageReference Filter()
    {
        opp=[SELECT AccountId,CloseDate,Name,Probability FROM Opportunity WHERE OwnerId=:UserInfo.getUserId() AND StageName=:stage]; 
        return NULL; 
    }
    public FilterOpportunity()
    {
        opp=[SELECT AccountId,CloseDate,Name,Probability FROM Opportunity WHERE OwnerId=:UserInfo.getUserId() AND StageName='Qualification']; 
    } 
}