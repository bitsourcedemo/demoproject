public with sharing class AwesomeProductController {
	@AuraEnabled
	public static List<Product__c> getProducts(){
		return [select id, name, photo__c, description__c, points__c from product__c];
	}

	@AuraEnabled
	public static Product__c getProductByName(String Name){
		return [select id, name, photo__c, color__c,
                points__c, description__c,
                (select name from Products_Size__r order by name)
                from product__c where name = :name];
	}
	public AwesomeProductController() {}
}