public with Sharing class SampleAccountDisplay 
{    
    public List<Account> acc {get;set;}
    public List<Contact> mem {get;set;}    
       
    public SampleAccountDisplay()
    {
        acc = [SELECT Name, AccountNumber FROM Account limit 1000];
        mem = [SELECT Name FROM Contact];
    }
}