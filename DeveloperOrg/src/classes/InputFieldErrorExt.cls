/*******************************************************************
 * Extension controller for the "Adding Errors to Input Fields" 
 * recipe. 
 * Adds validation errors to sobject fields that can be displayed
 * against the field. 
 *******************************************************************/
 
 Public with sharing class InputFieldErrorExt{
     
     //the Parent Standard Controller
     private Apexpages.StandardController stdCtrl;
     //system.debug('value near to the declaration  lalit1'+stdCtrl);
     
     //Constructor
     public InputFieldErrorExt(ApexPages.StandardController std)
     {
         stdCtrl =std;
         system.debug('value of the parent std controller inside the constructor lalit2'+stdCtrl);
     }
    
    // validates the inputs and adds error messages or delegates to
    // the standard controller save method
    
    public PageReference save()
    {
        PageReference result = null;
        Contact cont = (Contact) stdCtrl.getRecord();
        system.debug('inside the pagereference lalit3'+cont);
        if ( (String.IsBlank(cont.Email)) && (String.IsBlank(cont.Phone)) )
        {
            cont.email.addError('Please enter an email address or phone number');
            cont.phone.addError('Please enter a phone number or email address');
        }
        else
        {
            result=stdCtrl.save();
        }
        return result;
    }
 }