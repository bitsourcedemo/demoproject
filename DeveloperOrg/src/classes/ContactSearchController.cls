public with sharing class ContactSearchController {
    private String soql{get;set;} //soql without the limit and order
    
    //the collection of contacts to display.
    public List<Contact> contacts{get;set;}
    
    //current sort direction defaults to asc.
    public String sortDir{
        get{if(sortDir == null) {sortDir='asc';} return sortDir;} set;
    }
    // the current field to sort by. defaults to last name
    public String sortField{
        get{if(sortField == null){sortField = 'LastName';}return sortField;} set;
    }
    //format the soql for display on the visualforce page.
    //public String debugsoql{
        
    //}
}