public with sharing class locationController {
    Public List<Location__c> locationList{get;set;}
    
    public locationController()
    {
        locationList = [Select Id,City__c,Country__c,State__c From Location__c ];
    }
}