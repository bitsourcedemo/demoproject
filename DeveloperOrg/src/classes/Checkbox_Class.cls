public class Checkbox_Class {
    List<accountwrapper1> accountList = new List<accountwrapper1>();
    List<Account> selectedAccounts = new List<Account>();
    
    public List<accountwrapper1> getAccounts(){
        for(Account a : [select Id,Name,AccountNumber,Phone From Account Limit 5])
            accountList.add(new accountWrapper1(a));
        return accountList;
    }
    public PageReference getSelected(){
        selectedAccounts.clear();
        for(accountwrapper1 accwrapper : accountList)
            if(accwrapper.selected == True){
                selectedAccounts.add(accwrapper.acc);
            }
                        return null;
    }
    public List<Account> GetSelectedAccounts(){
        if(selectedAccounts.size() >0)
            return selectedAccounts;
        else 
            return null;
    }

    public class accountwrapper1{
        public Account acc{get;set;}
        public Boolean Selected{get;set;}
        
        public accountwrapper1(Account a){
            acc = a;
            Selected =False;
        }
    }
}