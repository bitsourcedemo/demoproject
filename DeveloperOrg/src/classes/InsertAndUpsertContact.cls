public with sharing class InsertAndUpsertContact {
	
	public static List<Contact> returnContact(){
		Contact josh = new Contact(FirstName='Josh',LastName='Kaplan',Department='Finance');
		insert josh;
		josh.Description ='Josh\'s record has been updated by the upsert operation';
		
		Contact kathy = new Contact(FirstName='kathy',LastName='metoyear',Department='Technology');
		
		List<Contact> contacts = new List<Contact>{josh,kathy};
		
		//calling upsert
		Upsert contacts;
		
		return contacts;
	}

}