public with sharing class RemoveLineItemsTriggerHandler
{
    public void onAfterInsert(List<Product2> newRecords)
    {
        updateOpportunity(newRecords);
    }

    public void onbeforeupdate(List<Product2> oldRecords,List<Product2> updatedRecords,Map<Id,Product2> newmap ,Map<Id,Product2> oldmap )
    {
        updateOpportunity(updatedRecords);
    }
    //updates the opportunity with the opportunity line items if the status of the opportunity is approve and removes if the status is reject
    private void updateOpportunity(List<Product2> newRecords)
    {
     List<Opportunity> newopp =[Select Id,Name,Status__c From Opportunity];
     Map<Id,String> opportunityMap = new Map<Id,String>();
      for(Opportunity opp :newopp)
      {
          for (Product2 soi : newRecords) 
          {
             if(opp.Status__c == 'Approve')
             {
                 opportunityMap.put(soi.Id,soi.Name);
             }
          }
      }
      List<Opportunity> lstopp = [Select Id,Name,Status__c From Opportunity where Id IN :opportunityMap.keyset()];
      
      for(Opportunity opp : lstopp)
      {
          opp.Status__c = opportunityMap.get(opp.Id);
      }
      upsert lstopp;
    }
}