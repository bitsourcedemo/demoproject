global class MyBatchable implements Database.Batchable<sObject>
{
    private final string initialState;
    String query;
    
    global MyBatchable(String initialState)
    {
        this.initialState = initialState;
    }
    global Database.queryLocator start(Database.BatchableContext BC)
    {
        return Database.getqueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> batch){
        
    }
    global void finish(Database.BatchableContext BC){
    }
}