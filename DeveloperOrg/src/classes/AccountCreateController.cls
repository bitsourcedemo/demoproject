public with sharing class AccountCreateController{

    public string name{get;set;}
    public string phone{get;set;}
    public string AccountNumber{get;set;}
    public string AccountSource{get;set;}
    public Long   AnnualRevenue{get;set;}
    public string BillingCity{get;set;}
    public string BillingCountry{get;set;}
    public string BillingPostalCode{get;set;}
    public string BillingState{get;set;}
    public string BillingStreet{get;set;}
    public string CleanStatus{get;set;}
    public string Description{get;set;}
    public string Industry{get;set;}
    public Integer NumberOfEmployees{get;set;}
    public string Rating{get;set;}
    public string Type{get;set;}
    
    
    public AccountCreateController(){}
    
    public PageReference  save()
    {
        Account a = new Account();
        a.Name = name;
        a.Phone = phone;
        a.AccountNumber = AccountNumber;
        a.AccountSource = AccountSource;
        a.AnnualRevenue = AnnualRevenue;
        a.BillingCity =BillingCity;
        a.BillingCountry = BillingCountry;
        a.BillingPostalCode = BillingPostalCode;
        a.BillingState = BillingState;
        a.BillingStreet = BillingStreet;
        a.CleanStatus = CleanStatus;
        a.Description = Description;
        a.Industry = Industry;
        a.NumberOfEmployees = NumberOfEmployees;
        a.Rating = Rating;
        a.Type = Type;
        
        insert a;
        return null;
    }
    public PageReference Cancel(){
            Pagereference pg = new Pagereference('/apex/AccountListView');
            pg.setRedirect(true);
            return pg;
    }
    
    public PageReference saveAndNew(){    
        PageReference pgRef = null;
        try{
            save();
            pgRef = Page.AccountNewCreate;
            pgRef.setRedirect(true);
        }catch(Exception ex){
            System.debug('####Error while Saving records #### ' + ex.getMessage());
             ApexPages.Message msg = new APexPages.Message(ApexPages.Severity.INFO, 'Error occurred');
           ApexPages.addMessage(msg);
        }
        return pgRef;
    }
}