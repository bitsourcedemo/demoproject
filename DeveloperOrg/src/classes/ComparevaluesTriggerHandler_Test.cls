@isTest
private class ComparevaluesTriggerHandler_Test
{
    static testmethod void compareValues()
    {
        Account a = new Account();
        a.Name='Test1';
        a.AccountNumber = '1234';
        a.Type = 'Customer - Direct';
        
        insert a;
        system.Assertequals(a.Type,[select Type From Account Where Id =:a.Id].Type );
        
        a.Name = 'Test1';
        a.AccountNumber = '3567';
        a.Type = 'Prospect';
        update a;
        
        system.AssertEquals(a.Type,[Select Type From Account Where Id =:a.Id].Type);
    
    }
}