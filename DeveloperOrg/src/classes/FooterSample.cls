//page name:FooterExample
//code is from infallible techie
public class FooterSample {
    public List<Account> listAccount {get;set;}
    public Double totalRevenue {get;set;}
    public FooterSample() {
        totalRevenue = 0;
        listAccount = [SELECT Name, AnnualRevenue FROM Account];
        calculateTotalRevenue();
    }
    private void calculateTotalRevenue() {
        for(Account acct : listAccount) {
            if(acct.AnnualRevenue != null)
                totalRevenue += acct.AnnualRevenue;
        }
    }
}