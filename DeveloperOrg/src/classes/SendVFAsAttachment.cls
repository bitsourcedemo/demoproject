/*
 * Created By : Lalit Singh
 * Description :   This class exposes @future method to send VF page rendered as attachment in Email. 
 */

public class SendVFAsAttachment {
    @future(callout=True)
    public static void sendVF(String EmailIdCSV, String Subject,String body,String userSessionId){
        String addr = 'https://ap1.salesforce.com/services/apexRest/sendPdfEmail';
        HttpRequest req = new HttpRequest();
        req.setEndpoint( addr );
        req.setMethod('Post');
        req.setHeader('Authorization','OAuth'+ userSessionId);
        req.setHeader('Content-Type','application/json');
        Map<String,String> postBody = new Map<String,String>();
        postBody.put('EmailIdCSV',EmailIdCSV);
        postBody.put('Subject',Subject);
        postBody.put('Body',body);
        String reqBody = JSON.serialize(postBody);
        req.setBody(reqBody);
        Http http = new Http();
        HttpResponse response = http.send(req);
    }
}