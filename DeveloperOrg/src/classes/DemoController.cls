Public with sharing class DemoController {
Public Id LoggedInUSerId{get;set;}
Public String LoggedInUserName{get;set;}
 
    Public DemoController(ApexPages.StandardController controller) {
     LoggedInUSerId = UserInfo.getUserId();
     LoggedInUserName = UserInfo.getUserName();
     
    }
}