//using this approach we can avoid the soql/sosl query in apex class/test class to fetch the recordtypeid
public class DynamicRecordTypeUtility{
    //this method will take sObject API as input.
    public static Map<String,schema.recordtypeinfo> recordTypeInfo(String objectApiName){
        Map<String, schema.sObjecttype> sobjectMap = Schema.getGlobalDescribe();
        system.debug('**sobjectMap**'+sobjectMap);
        Schema.SobjectType sObjecType = sObjectMap.get(objectApiName);
        system.debug('**sObjecType**'+sObjecType);
        Schema.DescribeSObjectResult SObjectTypeDescribe = sObjecType.getDescribe();
        system.debug('--SObjectTypeDescribe--'+SObjectTypeDescribe);
        return sObjectTypeDescribe.getRecordTypeInfosByName();
    }
}