//web services example with the util classes.
// @author : Patrick 

/** This class is a utility class for WebServices and other API classes */

global with sharing class APIUtils
{
     /** Return codes to for returnCdoe */
    
    global final static Integer STATUS_OK = 200;
    global final static Integer STATUS_CREATED = 201;
    global final static Integer STATUS_ACCEPTED = 202;
    global final static Integer STATUS_BAD = 400;
    global final static Integer STATUS_FORBIDDEN = 403;
    global final static Integer STATUS_NOTFOUND = 404;
    global final static Integer STATUS_NOTALLOWED = 405;
    global final static Integer STATUS_ISE = 500;

    public virtual class UnknownException extends GenericUtils.UnknownException{}
    public virtual class BadException extends GenericUtils.BadException{}
    
    /** This class is an abstraction of the Contact object */
    global class APIContact {
    WebService Integer returnCode;
    WebService String message;
    
    WebService String name;
    WebService Boolean isActive;
    
    /**
        * Blank constructor for the Contact
        */
        public APIContact() {}
        
     /**
        * Constructor based on a Contact object
        * 
        * @param contact A contact
        */
     
      public APIContact(Contact contact) {      
        this();
        this.name = contact.Name;
         this.isActive = contact.Is_User_Active__c;
      }
    }
    
     /** This class is an abstraction of the Account object */
     global class APIAccount {
     
        WebService Integer returnCode;
        WebService String message;

        WebService String name;
        WebService String accountNumber;
        WebService String myField;
        WebService Boolean isAwesome;
        WebService List<APIContact> contacts;

     /**
        * A blank constructor
        */
        public APIAccount() {}

/**
        * A constructor based on Account
        * @param account The account
        * @param awesomeness Is the account awesome
        */
        public APIAccount(Account account, Boolean awesomeness) {
            this();
            this.name = account.Name;
            this.accountNumber = account.AccountNumber;
            this.myField = account.MyField__c;
            this.isAwesome = awesomeness;
        }
        
        /**
        * A constructor based on Account
        * @param account The Account
        */
        public APIAccount(Account account) {
            this(account, true);
        }
     /**
        * Adds contacts to the account
        * 
        * @param contacts The contacts to add
        */
        public void addContacts(List<Contact> contacts) {
            this.contacts = (this.contacts != null) ? this.contacts : new List<APIContact>();

            for (Contact contact : contacts) {
                this.contacts.add(new APIContact(contact));
            }
        }
     
     }
     /** This is the context object for accounts */
     global class AccountContext 
     {
         public String accountNumber;
         
         private Account account {
            get {
                if (this.account == null) {
                    this.account = AccountUtils.getAccountByNumber(this.accountNumber);
                }

                return this.account;
            }
            set;
        }
         private List<Contact> contacts {
            get {
                if (this.contacts == null) {
                    this.contacts = ContactUtils.getContactsForAccount(this.account);
                }

                return this.contacts;
            }
            set;
        }
     
     /**
        * Blank constructor
        */
        public AccountContext() {}
        
        /**
        * Get the account
        * 
        * @return The account
        */
        public Account getAccount() {
            return this.account;
        }
        
        /**
        * Gets a list of contacts for the account
        * 
        * @return The contacts
        */
        public List<Contact> getContacts() {
            return this.contacts;
        }
        /**
    * Validates that a given account context is valid
    * 
    * @param context The context to check
    * @throws APIUtils.BadException if the context is not valid
    */
    }
      public static void ensureContext(AccountContext context) {
        if (context == null || GenericUtils.isBlank(context.AccountNumber)) {
            throw new BadException('Account number must be set');
        }
    }
    
}