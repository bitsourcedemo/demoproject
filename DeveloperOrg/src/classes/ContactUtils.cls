//web services example with the util classes.
// @author : Patrick 
public class ContactUtils{

    /**
    * Returns a list of contacts for a given account
    * 
    * @param account The account
    * @return The contacts for the account
    */
    public static List<Contact> getContactsForAccount(Account account)
    {
    
        return[
                Select Is_User_Active__c,name From Contact where AccountId =:account.Id];
    }
}