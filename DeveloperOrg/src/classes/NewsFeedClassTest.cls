@isTest
private class NewsFeedClassTest {

    static testMethod void myUnitTest() {
        // Build Simple Feed Item
        ConnectApi.FeedItemPage testPage = new ConnectApi.FeedItemPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        testItemList.add(new ConnectApi.FeedItem());
        testItemList.add(new ConnectApi.FeedItem());
        testPage.items = testItemList;
        
        // set the test data
        ConnectApi.ChatterFeeds.setTestGetFeedItemsFromFeed(null,ConnectApi.FeedType.News, 'me', testPage);
        // The method returns the test page, which we know has two items in it.
        
        Test.startTest();
        System.AssertEquals(2,NewsFeedClass.getNewsFeedCount());
        Test.StopTest();
    }
}