public class InsertObjectController 
{
 public Boolean initialised{get; set;}
 public Account acc {get; set;}
  
 public InsertObjectController()
 {
  initialised=false;
 }
  
 public void init()
 {
  if (!initialised)
  {
   acc=new Account();
   acc.Name='Blog Account';
   insert acc;
   initialised=true;
  }
 }
}