public class myWeb2LeadExtension {
    private final Lead weblead;
    
    public myWeb2LeadExtension(ApexPages.StandardController stdController){
        weblead = (Lead)stdController.getRecord();
    }
    
    public PageReference saveLead(){
        try{
            insert(weblead);
        }catch(System.DmlException e ){
            Apexpages.addMessages(e);
            return null;
        }
        pageReference p = Page.Thank_you;
        p.setRedirect(true);
        return p;
    }
}