global class accountOperation implements Database.Batchable<sObject>{
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string rating = 'Hot';
        String query = 'SELECT Id, Name, CustomerPriority__c, Type, Rating FROM Account where Rating = :rating ';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> accList){
        
        for(Account acc : accList){
            if(acc.Name != null){
                acc.Name = 'UpdatedByBatch';
                acc.Active__c = 'Yes';
            }
            try{
                update accList;
            }
            catch(exception ex){
                System.debug(ex);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
  }
}