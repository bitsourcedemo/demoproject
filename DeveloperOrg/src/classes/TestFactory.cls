@istest public class TestFactory
{
    public static User buildTestUser(Integer i,String profilename)
    {
        User newuser = new User();
        newuser.LastName=profilename+i;
        newuser.Alias='User'+i;
        newuser.Email='lalitsngh@yahoo.com';
        newuser.UserName='test_'+i+'@yahoo.com';
        newuser.CommunityNickname='test_'+i+'@yahoo.com';
        newuser.ProfileId=[Select id from Profile WHERE Name =:profilename].Id;
        newuser.TimeZoneSidKey ='America/Los_Angeles';
        newuser.LocaleSidKey='en_US';
        newuser.EmailEncodingKey='UTF-8';
        newuser.LanguageLocaleKey='en_US';
        newuser.isActive=true;
        //newuser.People_Manager__C=true;
        newuser.Region__c ='West';
        return newuser;
    }
    public static Position__c buildTestPosition(Integer i,Id testUserId)
    {
        Position__c newposition = new Position__c();
        newposition.Name='TestPosition'+i;
        newposition.Status__c = 'New';
        newposition.Job_Description__c='Does Stuff';
        newposition.Hiring_Manager__c =testUserId;
        newposition.OwnerId = testUserId;
        return newposition;
    }


}