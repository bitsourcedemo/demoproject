//this is the controller for APEX SEARCH WITH CHECKBOX RESULTS
/*
    This demo is a single Apex custom controller, 
    two Visualforce pages and a wrapper class allowing the user to search an object by keyword (via Dynamic SOQL) 
    and return the results in a page block table with corresponding checkboxes.
     Selecting one or more checkboxes and clicking the 'See Results' button displays the list of the selected items.
     
     
     visual force page it is using : Category_Search,Category_Results 
*/
public class CategorySearchController {

    // the results from the search. do not init the results or a blank rows show up initially on page load
    public List<categoryWrapper> searchResults {get;set;}
    // the categories that were checked/selected.
    public List<categoryWrapper> selectedCategories {
        get; //{
            //if (selectedCategories == null) selectedCategories = new List<categoryWrapper>();
            //return selectedCategories;
        //};
        set;
    }      

    // the text in the search box
    public string searchText {
        get; //{
            //if (searchText == null) searchText = 'Category'; // prefill the serach box for ease of use
            //return searchText;
        //};
        set;
    } 

    // constructor
    public CategorySearchController() {}

    // fired when the search button is clicked
    public PageReference search() {

        if (searchResults == null) {
            searchResults = new List<categoryWrapper>(); // init the list if it is null
        } else {
            searchResults.clear(); // clear out the current results if they exist
        }
        
        // dynamic soql for fun
        String qry = 'Select Name,Id,Industry,Type From Account  Where Name LIKE \'%'+searchText+'%\' Order By Name';
        // may need to modify for governor limits??
        for(Account c : Database.query(qry)) {
            // create a new wrapper by passing it the category in the constructor
            CategoryWrapper cw = new CategoryWrapper(c);
            // add the wrapper to the results
            searchResults.add(cw);
        }
        return null;
    }   

    public PageReference next() {

        // clear out the currently selected categories
        selectedCategories.clear();

        // add the selected categories to a new List
        for (CategoryWrapper cw : searchResults) {
            if (cw.checked)
                selectedCategories.add(new CategoryWrapper(cw.acc));
        }

        // ensure they selected at least one category or show an error message.
        if (selectedCategories.size() > 0) {
            return Page.Category_Results;
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one Category.'));
            return null;
        }       

    }       

    // fired when the back button is clicked
    public PageReference back() {
        return Page.Category_Search;
    }       

}