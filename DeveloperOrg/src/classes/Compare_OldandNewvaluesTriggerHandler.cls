public with sharing class Compare_OldandNewvaluesTriggerHandler{

    public void themethod(List<Account> newList , List<Account> oldList)
    {
    
            for (Account accNew: newList)
                {
                    for(Account accOld :oldList)
                    {
                        if(accNew.AccountNumber != accOld.AccountNumber)
                        {
                            accNew.Type='Prospect';
                        }
                        else
                        {
                            accNew.Type = 'Customer - Direct';
                        }
                    }
                }
    }
}