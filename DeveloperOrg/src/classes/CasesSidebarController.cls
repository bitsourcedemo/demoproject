public with sharing class CasesSidebarController 
{
    Public Map<String,Integer> CaseCountbyStatus{get;set;}
    
    public CasesSideBarController()
    {
        List<String> ClosedLabels = new List<String>();
        
        for(CaseStatus cand:[Select MasterLabel From CaseStatus where IsClosed=True])
        {
            ClosedLabels.add(cand.MasterLabel);
        }
        caseCountByStatus =  new Map<String,Integer>();
        for(AggregateResult ar:[select status, count(id) caseCount from Case where status NOT IN :closedLabels GROUP BY status])
        {
            caseCountByStatus.put((String) ar.get('Status'),(Integer)ar.get('CaseCount'));
        }
}
}