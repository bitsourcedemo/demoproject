/*
Created By: Lalit Singh

Created Date: 11-11-2014

Release:R1

Capability: Knowledege Management

Description: This custom controller contains the logic to show the published article page in pdf and convert that page into Base 64.

Revision

Version 1:Lalit Singh - 11-November-2014 - Initial version

*/

@RestResource(urlMapping='/v1/Articles/*')

global with sharing class RESTArticleController
{
     @HttpGet
        global static List<String> showArticles()       
        {      
             blob articlePdf;
             List<ID> artId;
             String stringEncoded;
             List<String> addConvPDF;
             artId = new List<ID>();
             List<KnowledgeArticleVersion> lstArticles = new List<KnowledgeArticleVersion>([select id from KnowledgeArticleVersion where publishStatus = 'Online' and Language = 'en_US' limit 2]);
             for(KnowledgeArticleVersion lstNewArticles :lstArticles)
            {
                if(lstNewArticles != null)
                {
                   artId.add(lstNewArticles.id);
                }
            }
             addConvPDF = new List<String>();
             pageReference kmPDF;
             
             for(ID kId : artId)
             {
                 kmPDF = new pageReference('/'+kId);
                 
                 articlePDF = kmPDF.getContentAsPDF();
                 
                 stringEncoded = EncodingUtil.Base64Encode(articlePDF);
                 addConvPDF.add(stringEncoded);
             }
                 return addConvPDF;  
        }
}