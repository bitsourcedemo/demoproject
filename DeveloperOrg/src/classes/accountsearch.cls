public with sharing class accountsearch  {
    public List<Account>searchResults {get;set;}
    public string searchText{get;set;}
    public accountsearch(ApexPages.StandardController controller) { }
    public pageReference search(){
        String qry = 'select Name,BillingCity,Phone,Website from Account '+' Where Name LIKE \'%'+searchText+'%\' order by Name';
        SearchResults = Database.query(qry);
        if(SearchResults.size() == 0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,'No <span id="IL_AD7" class="IL_AD">Accounts</span> Found'));
        }
        return null;
    }
}