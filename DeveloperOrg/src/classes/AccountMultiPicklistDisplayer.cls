public class AccountMultiPicklistDisplayer {
    public String picklistvalue{get;set;}
    List<Account> lstAccount = new List<Account>();
    List<SelectOption> optionValue = new List<SelectOption>();
    public String picklistValueDisplayer(){
        lstAccount = ([SELECT Id,Multi_Picklist__c,Name FROM Account]);
        return null;
    }
    
    public List<Selectoption> getCompareValues()
    {
        Schema.DescribeFieldResult picklistFieldValue = Schema.sObjectType.Account.fields.Multi_Picklist__c;
        system.debug('picklistFieldValue : '+picklistFieldValue);
        Schema.PicklistEntry [] PicklistValues = picklistFieldValue.getPickListValues();
        system.debug('PicklistValues : '+PicklistValues);
        for(Schema.PicklistEntry val : PicklistValues)
        {
            optionValue.add(new SelectOption(val.getValue(), val.getLabel()));
        }
        system.debug('optionValue : '+optionValue);
        return optionValue;
    }
}