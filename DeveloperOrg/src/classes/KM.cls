public with sharing class KM
{
    public Blob articlePDF{get;set;}
    public List<ID> artId;
    public List<String> addConvPDF;
    public KnowledgeArticleVersion lstArticles;
    public KM()
    {
    artId = new List<ID>();
    ApexPages.currentPage().getParameters().get('id');
    List<KnowledgeArticleVersion> lstArticles = new List<KnowledgeArticleVersion>([select id from KnowledgeArticleVersion where publishStatus = 'Online' and Language = 'en_US' limit 2]);
    for(KnowledgeArticleVersion lstNewArticles :lstArticles)
    {
        if(lstNewArticles != null)
        {
           artId.add(lstNewArticles.id);
        }
    }
    
    }
    public pageReference kmPDF()
    {
            
            addConvPDF=new List<String>(); 
            pageReference kmPDF;
            for(ID kad : artId){
            kmPDF = new pageReference('/'+kad);
            articlePDF=kmPDF.getContentAsPDF();
            String s= EncodingUtil.Base64Encode(articlePDF);
            addConvPDF.add(s);
            }
            return null;
  
            
    }
    
    public List<String> getPDFVal(){
    return addConvPDF;
    }
    
}