public class ActionParameterController {
    
    public Id oppIdToWin{get;set;}
    
    public List<Opportunity> getopps()
    {
     return [Select id,Name,StageName,CloseDate,Amount from Opportunity limit 50];   
    }
    
    public pagereference winopp(){
        Opportunity opp = new Opportunity(Id = oppIdToWin,StageName ='Closed Won');
        update opp;
    	return null;
    }

}