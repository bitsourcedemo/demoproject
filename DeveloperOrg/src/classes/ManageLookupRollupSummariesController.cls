public with sharing class ManageLookupRollupSummariesController {

	public LookupRollupSummary__mdt LookupRollupSummary {get;set;}

	public String selectedLookup {get;set;}

	public ManageLookupRollupSummariesController() {
		LookupRollupSummary = new LookupRollupSummary__mdt();
	}

	public List<SelectOption> getLookups() {
		// List current rollup custom metadata configs
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('[new]','Create new...'));
		for(LookupRollupSummary__mdt rollup : 
				[select DeveloperName, Label from LookupRollupSummary__mdt order by Label])
			options.add(new SelectOption(rollup.DeveloperName,rollup.Label));
		return options;
	}

	public PageReference init() {
		// URL parameter?
		selectedLookup = ApexPages.currentPage().getParameters().get('developerName');		
		if(selectedLookup!=null)
		{
			LookupRollupSummary = 
				[select 
					Id,
					Label,
					Language,
					MasterLabel,
					NamespacePrefix,
					DeveloperName,
					QualifiedApiName,
					Parent_Object__c,
					Relationship_Field__c,
					Child_Object__c,
					Relationship_Criteria__c,
					Relationship_Criteria_Fields__c,
					Field_to_Aggregate__c,
					Field_to_Order_By__c,
					Active__c,
					Calculation_Mode__c,
					Aggregate_Operation__c,
					Calculation_Sharing_Mode__c,
					Aggregate_Result_Field__c,
					Concatenate_Delimiter__c,
					Calculate_Job_Id__c,
					Description__c 
				from LookupRollupSummary__mdt 
				where DeveloperName = :selectedLookup];
		}
		return null;
	}

	public PageReference load() {
		// Reload the page
		PageReference newPage = Page.managelookuprollupsummaries;
		newPage.setRedirect(true);
		if(selectedLookup != '[new]')
			newPage.getParameters().put('developerName', selectedLookup);
		return newPage;
	}

	/*public PageReference save() {
		try {
			// Insert / Update the rollup custom metadata
			if(LookupRollupSummary.Id==null)
				CustomMetadataService.createMetadata(new List<SObject> { LookupRollupSummary });
			else
				CustomMetadataService.updateMetadata(new List<SObject> { LookupRollupSummary });
			// Reload this page (and thus the rollup list in a new request, metadata changes are not visible until this request ends)
			PageReference newPage = Page.managelookuprollupsummaries;
			newPage.setRedirect(true);
			newPage.getParameters().put('developerName', LookupRollupSummary.DeveloperName);
			return newPage;			
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return null;
	}*/

	/*public PageReference deleteX() {
		try {
			// Delete the rollup custom metadata
			CustomMetadataService.deleteMetadata(
				LookupRollupSummary.getSObjectType(), new List<String> { LookupRollupSummary.DeveloperName });
			// Reload this page (and thus the rollup list in a new request, metadata changes are not visible until this request ends)
			PageReference newPage = Page.managelookuprollupsummaries;
			newPage.setRedirect(true);
			return newPage;			
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return null;
	}*/	
}