/*******************************************************************
 * Custom controller for the "Passing Parameters between Visualforce
 * Pages" recipe.
 * Retrieves the accounts whose ids are present on the URL and allows
 * these to be edited.
 *******************************************************************/
public with sharing class EditFromSearchController 
{
    // the accounts to be edited
    public List<Account> accs {get; set;}
    
    public String returnURL;
    
    // constructor - extracts account* parameters from the URL and
    // retrieves the associated account records
    public EditFromSearchController()
    {
        returnURL = Apexpages.currentPage().getParameters().get('account1');
        List<Id> ids=new List<Id>();
        Integer idx=1;
        String accStr;
        do 
        {
            accStr=ApexPages.currentPage().getParameters().get('account' + idx);
            if (accStr!=null)
            {
                ids.add(accStr);
            }
            idx++;
        }
        while (null!=accStr);
        
        if (ids.size()>0)
        {
            accs=[select id, Name, Industry, Type,Rating from Account where id in :ids];
        }
    }
    
    // saves the account records and redirects to the accounts tab
    public PageReference save()
    {
        update accs;
        return new PageReference('/001/o');
    }
    
    public pagereference doCancel(){
        pageReference page = new pageReference('/apex/SearchFromURL?id='+returnURL);
        //PageReference page = new PageReference('/'+returnURL+'/e?retURL=%2Fa'+returnURL);
        page.setRedirect(true);
        return page;
    }      
}