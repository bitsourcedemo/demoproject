public class ContactsListController {
    private string sortOrder ='FirstName';
    public List<Contact> getContacts()
    {
        List<Contact> results= Database.Query('SELECT Id, FirstName, LastName, Title, Email ' +'FROM Contact ' +'ORDER BY ' + sortOrder + ' ASC ' +'LIMIT 20');
        return results;
    }
    public void sortByLastName(){
        this.sortOrder = 'LastName';
    }
    public void sortByFirstName(){
        this.sortOrder = 'FirstName';
    }
}