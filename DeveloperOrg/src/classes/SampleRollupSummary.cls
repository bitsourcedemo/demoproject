public class SampleRollupSummary {
   public static void rollupOpportunities(List<Opportunity> opportunities) {  
        Set<Id> accountIds = new Set<Id>();
    
        //Get account Ids from specified opportunities
        for (Opportunity opp : opportunities) {
            accountIds.add(opp.AccountId);
        }
    
        if (accountIds.isEmpty() == false) {
            /*Execute as a future call so that the user doesn't have to wait around for
            the rollup to finish. Unless, already in a future or batch call state then
            just perform the rollup.*/
            if (System.isFuture() == false && System.isBatch() == false) {
                summarizeOpportunitiesAsync(accountIds);
            }
            else {
                new SampleRollupSummary().summarizeOpportunities(accountIds);
            }
        }
    }
    
    @future  
	public static void summarizeOpportunitiesAsync(Set<Id> accountIds) {  
		new SampleRollupSummary().summarizeOpportunities(accountIds);
	}

	public void summarizeOpportunities(Set<Id> accountIds) {  
		//Get Accounts to Update
		List<Account> accounts = queryAccountsById(accountIds);

		Map<Id, double> results = getOpportunityAmountsByAccountId(accountIds);

		//Loop Accounts and set Won Amount
		List<Account> accountsToUpdate = new List<Account>();
		for (Account acct : accounts) {
			double total = 0;

			if (results.containsKey(acct.Id)) {
				total = results.get(acct.Id);
			}

			//Determine if Total Amount has Changed
			if (acct.Total_Amount_Won__c != total) {
				acct.Total_Amount_Won__c = total;
				accountsToUpdate.add(acct); //Add account to collection to be updated
			}
		}

		if(accountsToUpdate.isEmpty() == false) {
			Database.SaveResult[] saveResults = Database.update(accountsToUpdate, false);
			System.debug(saveResults);
		}
	}
	
	//Private Methods
    public Map<Id, double> getOpportunityAmountsByAccountId(Set<Id> accountIds) {
        Map<Id, double> resultsByAccountId = new Map<Id, double>();

        //Summarize Won Opportunity Amounts by Account Id
        AggregateResult[] results = aggregateOpportunityAmounts(accountIds);
        for (AggregateResult result : results) {
            Id accountId = (Id) result.get('Account');
            double total = (double) result.get('Total');

            resultsByAccountId.put(accountId, total);
        }
        return resultsByAccountId;
    }
    
	//Query Methods
    private List<Account> queryAccountsById(Set<Id> accountIds) {
        return [SELECT Id, Total_Amount_Won__c FROM Account WHERE Id IN :accountIds];
    }
    
    private AggregateResult[] aggregateOpportunityAmounts(Set<Id> accountIds) {
        return [SELECT AccountId Account,SUM(Amount) Total FROM Opportunity WHERE IsClosed = true AND IsWon = true AND AccountId IN :accountIds GROUP BY AccountId];
    }
}