/*******************************************************************
 * Custom class to encapsulate a String property - used in the
 * "Multi-Select Related Controller" recipe.
*******************************************************************/
public class StringContainer {
    //the string encapsulated
    Public String value{get;set;}
    
    // Constructor-takes the string to encapsulates
    Public StringContainer(String inVal){
        value = inVal;
    }

}