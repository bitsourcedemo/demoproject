public with sharing class AccountListViewController{
        
        public List<checkBox> checkBoxWrapper {get;set;}
        
        public AccountListViewController()
        {
            List<Account> listAcct = [SELECT AccountNumber,AccountSource,AnnualRevenue,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,CleanStatus,Description,Industry,Name,NumberOfEmployees,Phone,Rating,Type FROM Account LIMIT 1000];
        
            if(listAcct.size() > 0)
            {
                checkBoxWrapper = new List<checkBox>();
            
                for(Account a : listAcct)
                {
                    checkBoxWrapper.add(new checkBox(a));
                }
        
            }
        }
        Public List<Account> getlstAccount()
        {
            List<Account> lstAccount = New List<Account>();
            lstAccount = ([SELECT AccountNumber,AccountSource,AnnualRevenue,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,CleanStatus,Description,Industry,Name,NumberOfEmployees,Phone,Rating,Type FROM Account]);
            return lstAccount;
        }
        
        public PageReference NewPage()
        {
        
   
            Pagereference pg = new Pagereference('/apex/AccountNewCreate');
            pg.setRedirect(true);
            return pg;
        
        }
        
        public class checkBox
        {
            public Boolean checkBool {get;set;}
            public Account acct {get;set;}
            public checkBox(Account acct)
            {
                this.acct = acct;
            }
       }        
        
}