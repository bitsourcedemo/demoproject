public class TimeSheetTriggerHandler {
      public void splitTimeEntries(List<Account> accountList){
          
          String test = System.Label.RecordtypeValues;
          set<String> strrecordtypenames = new Set<String>();
          strrecordtypenames.addAll((System.Label.RecordtypeValues).split(';'));
          map<id,recordtype> mapofrecd = PocRecordTypetriggerHandler.returnRecordtypeValur(strrecordtypenames);
          
          for(Account account : accountList){ 
               if(mapofrecd.containsKey(account.RecordTypeId)){
                   system.debug('account.RecordTypeId : '+account.RecordTypeId);
                   account.Value__c = account.RecordTypeId + 'ServiceCaseRecordTypeId';
               }
               else{
                   system.debug('account.RecordTypeId : '+account.RecordTypeId);
                   account.Value__c = 'Well Done';
               }
               }
          }
}