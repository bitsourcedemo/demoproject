//Standard set controller demo
public with sharing class testStandardController {
    public testStandardController(ApexPages.StandardsetController Controller){
        Controller.setPageSize(2);
        Controller.last();
    }
  }