public class PartialSuccess {
	
	public void theMethodPartial()
	{
	// Create a list of contacts
	List<Contact> conList = new List<Contact> {new Contact(FirstName='Santi',LastName='Cazrola',Department='Finance'),new Contact(FirstName='Sadio',LastName='Mane',Department='Finance'),new Contact(FirstName='Roy',LastName='keane',Department='Finance')};
	
	Database.SaveResult[] srList = Database.insert(conList, false);
	for(Database.SaveResult sr : srList)
	{
		if(sr.isSuccess()){
			System.debug('Successfully inserted contact. Contact ID: ' + sr.getId());
		}else{
			for(Database.Error err : sr.getErrors()) {
				System.debug('The following error has occurred.');
				System.debug(err.getStatusCode() + ': ' + err.getMessage());
				System.debug('Contact fields that affected this error: ' + err.getFields());
			}
		}
	}
}	
}