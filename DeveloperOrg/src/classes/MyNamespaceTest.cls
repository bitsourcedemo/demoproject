@RestResource(urlMapping='/namespaceExample/*')
    global class MyNamespaceTest {
        @HttpPost
            global static MyUDT echoTest(MyUDT def, String extraString)
            {
                return def;
            }
            global class MyUDT
            {
                Integer count;
            }
}