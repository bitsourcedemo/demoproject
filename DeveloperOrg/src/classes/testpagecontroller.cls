public class testpagecontroller {
    public attachment att{get;set;}
    public transient blob b{get;set;}
    public testpagecontroller(){
        att = new attachment();
    }
    public void saveAtt(){
        system.debug(b);
        //need to pass the id of the object record where we want to attach the document in salesforce 
        att.parentid = '0019000001ZGXNy';
        att.Name = 'MyAttachment';
        insert att;
        att = null;
    }
}