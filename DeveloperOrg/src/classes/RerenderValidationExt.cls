/*******************************************************************
 * Extension controller for the "Errors - Harmful if Swallowed" 
 * recipe. Associates the chosen account with the opportunity when
 * the user populates a lookup.  
 *******************************************************************/
 
 public with sharing class RerenderValidationExt
 {
     //parent standard controller
     private ApexPages.StandardController stdCtrl;
     
     //constructor
     public RerenderValidationExt(ApexPages.StandardController std){
         stdCtrl = std;
         system.debug('value of stdCtrl :'+stdCtrl);
     }
     
     // action method invoked when the account is selected. Queries back the
    // selected account and attaches it to the opportunity, or clears the
    // account relationship if the account field has been cleared.
    public void AccountSelecected(){
        Opportunity opp = (Opportunity)stdCtrl.getRecord();
        system.debug('value of opp inside the method  '+opp);
        // handle the situation where the account field has been cleared
        if(!String.isBlank(opp.AccountId)){
            opp.Account = [select Website,Phone from Account where Id = :opp.AccountId];
        }
        else{
            opp.Account = Null;
        }
    }
 }