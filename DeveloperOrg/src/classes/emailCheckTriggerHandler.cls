public with sharing class emailCheckTriggerHandler {

    public void mailcheck(List<Contact> newList , List<Contact> oldList)
    {
        Map<Id,Contact> o = new Map<Id,Contact>();
        for(Contact c :oldList)
        {
            o.put(c.Id,c);
        }
        system.debug('value of o<<<<<<<<<<<<<<<<'+o);
        
        for(Contact n : newList ) //new values of contact
        {
            Contact old = new Contact();
            old = o.get(n.Id);  //mymap.get(a.name);
            system.debug('valur of old<<<<<<<<<<<<<<<<'+old);
            if(n.Email != old.Email)
            {
                n.Email.addError('Email cannot be changed');
            }
        }
    
    }
}