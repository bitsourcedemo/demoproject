//functionality : How to read a csv file and show it on a Visualforce page.
//Page Name : pageofReadingCsv

public class ReadingCsv{
    public Blob fileForExport {get;set;}
    public Boolean showBool {get;set;}
    public String output {get;set;}
    public List<List<String>> listOutput {get;set;}
    
    public ReadingCsv() {
        showBool = false;
        listOutput = new List<List<String>>();        
    }
    public void show() {
        showBool = true;
        String csvBody = EncodingUtil.base64Encode(fileForExport);
        
        //output = csvBody.toString();
        output = csvBody;
        system.debug('value after applying the tostring method'+output);
        List<String> tempList = new List<String>();
        tempList = output.split('\n');
        system.debug('value of templist'+tempList);
        for(String str : tempList)
        {
            listOutput.add(str.split(','));
            system.debug('--------------------------'+listOutput);
        }
    }
}