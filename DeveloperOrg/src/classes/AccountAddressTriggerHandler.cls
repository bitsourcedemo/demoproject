public class AccountAddressTriggerHandler {
    public static void methodForAddress()
    {
    	List<Account> lstacct = new List<Account>([Select BillingPostalCode,ShippingPostalCode From Account]);
        for(Account a : lstacct){
            If(a.BillingPostalCode!= null && a.Match_Billing_Address__c == True){
                a.ShippingPostalCode = a.BillingPostalCode;
                //lstacct.add(a);
            }
        }
    }
}