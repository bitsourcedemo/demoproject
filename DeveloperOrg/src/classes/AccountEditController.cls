public with sharing class AccountEditController {

    private final Account accountToUpdate;
    
    public AccountEditController(){
        accountToUpdate = [SELECT Id, Name,Phone,AccountNumber,AccountSource,AnnualRevenue,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,CleanStatus,Description,Industry,NumberOfEmployees,Rating,Type FROM Account WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    public Account getAccount() {
        return accountToUpdate;
    }
    
    public PageReference save(){
        update accountToUpdate;
        return null;
    }
}