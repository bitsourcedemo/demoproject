global  class JobSharingRecalc implements Database.Batchable<sObject>{
    global static String emailAddress = 'lalit.singh2@hp.com';
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT Id, Hiring_Manager__c, Recruiter__c FROM Job__c]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Map<ID, Job__c> jobMap = new Map<ID, Job__c>((List<Job__c>)scope);
        List<Job__Share> newJobShrs = new List<Job__Share>();
        List<Job__Share> oldJobShrs = [SELECT Id FROM Job__Share WHERE Id IN :jobMap.keySet() AND(RowCause = :Schema.Job__Share.rowCause.Recruiter__c OR RowCause = :Schema.Job__Share.rowCause.Hiring_Manager__c)];
        for(Job__c job : jobMap.values()){
        Job__Share jobHMShr = new Job__Share();
        Job__Share jobRecShr = new Job__Share();
        jobHMShr.UserOrGroupId = job.Hiring_Manager__c;
        jobHMShr.AccessLevel = 'Read';
        jobHMShr.ParentId = job.Id;
        jobHMShr.RowCause = Schema.Job__Share.RowCause.Hiring_Manager__c;
        newJobShrs.add(jobHMShr);
        jobRecShr.UserOrGroupId = job.Recruiter__c;
        jobRecShr.AccessLevel = 'Edit';
        jobRecShr.ParentId = job.Id;
        jobRecShr.RowCause = Schema.Job__Share.RowCause.Recruiter__c;
        newJobShrs.add(jobRecShr);
    }
    try {
        Delete oldJobShrs;
        Database.SaveResult[] lsr = Database.insert(newJobShrs,false);
        for(Database.SaveResult sr :lsr){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                    if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION && err.getMessage().contains('AccessLevel'))){
                    Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
                    String[] toAdresses = new String[] {emailAddress};
                    mail.setToAddresses(toAdresses);
                    mail.setSubject('Apex Sharing Recalculation');
                    mail.setPlainTextBody('The Apex sharing recalculation threw the following exception: ' +err.getMessage());
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
    }   
 }Catch(DmlException ex){
    Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {emailAddress};
    mail.setToAddresses(toAddresses);
    mail.setSubject('Apex Sharing Recalculation Exception');
    mail.setPlainTextBody('The Apex sharing recalculation threw the following exception: ' +ex.getMessage());
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
    }
  }
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {emailAddress};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Sharing Recalculation Completed.');
        mail.setPlainTextBody('The Apex sharing recalculation finished processing');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
    }
}