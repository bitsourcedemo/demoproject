public class CategoryWrapper {

    public Boolean checked{ get; set; }
    public Account acc { get; set;}

    public CategoryWrapper(){
        acc = new Account();
        checked = false;
    }

    public CategoryWrapper(Account a){
        acc = a;
        checked = false;
    }
/*
    public static testMethod void testMe() {

        CategoryWrapper cw = new CategoryWrapper();
        System.assertEquals(cw.checked,false);      

        CategoryWrapper cw2 = new CategoryWrapper(new Account(name='Test1'));
        System.assertEquals(cw2.cat.name,'Test1');
        System.assertEquals(cw2.checked,false);       

    }*/

}