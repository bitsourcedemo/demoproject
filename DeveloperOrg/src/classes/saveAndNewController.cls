//page name :samplenewandsave
//functionality : save and cancel button are standard and saveandnew is from the custom controller.
public with sharing class saveAndNewController
{
      ApexPages.StandardController sController;
     
       public saveAndNewController(ApexPages.StandardController controller) {
        sController = controller;
    }
    
    public pageReference saveAndNew(){
         sController.save();
         PageReference pg = new PageReference('/apex/samplenewandsave');
         pg.setRedirect(true);
         return pg;
    }

}