//Page Name :stControllerDemo
public class TodosController
{

  public ApexPages.StandardSetController stdSetController{get
  {
      if(stdSetController == null)
      {
         stdSetController = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Name,Type FROM Account]));
      }
         return stdSetController;
            } set;}
  public List<Account> getTodos() {
    return this.stdSetController.getRecords();
  }

  public void saveUpdates()
  {
    this.stdSetController.save();
  }
}