public class TestApproval 
{
	void submitAndProcessApprovalRequest(){
		// inserting and account
		
		Account acctoinsert = new Account(Name='david luiz',annualRevenue= 100.0);
		insert acctoinsert;
		
		User User1 = [Select Id From User Where Alias ='SomeStandarduser'];
		
		// create an approval request for the account
		Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
		req1.setComments('Submitting request for approval.');
		req1.setObjectId(acctoinsert.Id);
		
		// submit on behalf of a specific submitter
		req1.setSubmitterid(User1.Id);
		
		
		//submit the record to specific process and skip the criteria evaluation
		req1.setProcessDefinitionNameOrId('PTO_Request_Process');
		req1.setSkipEntryCriteria(true);
		
		//submit the approval request for the account
		Approval.ProcessResult result = Approval.process(req1);
		
		// verify the result
		System.Assert(result.IsSuccess());
		
		System.AssertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());
		
		//approve the submitted request
		//first , get the id of the newly created item
		List<Id> newWorkItemIds = result.getNewWorkitemIds();
		
		// Instantiate the new ProcessWorkitemRequest object and populate it
		Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
		req2.setComments('Approving Request...');
		req2.setAction('Approve');
		req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
		
		
		// Use the ID from the newly created item to specify the item to be worked
		req2.setWorkitemId(newWorkItemIds.get(0));
		
		//submit the request for approval
		Approval.ProcessResult result2 = Approval.process(req2);
		
		// verify the result
		System.Assert(result2.IsSuccess(),'Result Status:'+result2.isSuccess());
		System.assertEquals('Approved', result2.getInstanceStatus(),'Instance Status'+result2.getInstanceStatus());
	}

}