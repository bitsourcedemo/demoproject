/*******************************************************************
 * Extension controller for the "Styling Fields as Required" recipe.
 * Manages a contact and validates upon saving.
 *******************************************************************/
 public with sharing class RequiredStylingExt
 {
     Boolean error;
     // the standard controller that is being extended
     private ApexPages.StandardController stdCtrl;
     
     //Constructor
     public RequiredStylingExt(ApexPages.StandardController std){
         stdCtrl = std;
     }
     
     Public PageReference save(){
     	Contact con = (Contact)stdCtrl.getRecord();
     	system.debug('value of contact : '+con);
     	error = false;
     	
     	if(String.Isblank(con.FirstName)){
     		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the contact name'));
     		error = true;
     	}
     	
     	if(String.IsBlank(con.Email) && (String.IsBlank(con.Phone))){
     		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please supply the email address or phone number'));
     		error = true;
     	}
     	
     	PageReference result = null;
     	if(!error){
     		result = stdCtrl.save();
     	}
     	return result;
     }
     Public PageReference View(){
     	pageReference viewvalue;
     	return viewvalue;
     }
  }