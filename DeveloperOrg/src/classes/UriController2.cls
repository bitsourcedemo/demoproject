public class UriController2 {
    //*********************************************************************************
    // *Tip : Get the PageParameters in a Map and get key,values pairs smoothly of this 
    //*********************************************************************************
    
    // URi to read - http://test.salesforce.com/apex/helloworld?q=texas&city=Austin&zip=78666&ext=1234
    
    private string thequery {get; set;}
    private string thecity { get; set;}
    private string thezip { get; set;}
    private string theext { get; set;}
    
    //Map of PageParameters 
    private Map<string,string> pageParams=ApexPages.currentPage().getParameters();
    
    //Set the variable in Constructor
    public UriController2() 
    {
      //get each variable through Map
     thequery = ApexHelper.GetValueFromParam('q');
     thecity = ApexHelper.GetValueFromParam('city');
     thezip = ApexHelper.GetValueFromParam('zip');
     theext = ApexHelper.GetValueFromParam('ext');
    }
    }