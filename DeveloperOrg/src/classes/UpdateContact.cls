Public Class UpdateContact
{
    public void themethod(List<Account> lstAccount)
    {
        List<Contact> con = new List<Contact>();
        for(Account acc:lstAccount)
        {
            if(acc != null)
            {
                con.add(new Contact(FirstName =acc.Name,LastName='Email',Email= acc.CompanyEmail__c,AccountId = acc.Id,Fax=acc.Fax,MailingStreet=acc.BillingStreet,
            	MailingCity=acc.BillingCity,MailingState=acc.BillingState,MailingPostalCode=acc.BillingPostalCode,
            	MailingCountry=acc.BillingCountry,Phone=acc.Phone)); 
            }
            if(!con.isEmpty())
            {
    			 insert con;        	
            }
            Attachment attachment = new Attachment();
    		attachment.Body = Blob.valueOf('salesforce is an #1 CRM');
    		attachment.Name = String.valueOf('test.txt');
    		attachment.ParentId = con[0].Id;
    		insert attachment;
        }
    }
}