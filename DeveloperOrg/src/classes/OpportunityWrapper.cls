global class OpportunityWrapper implements Comparable {

	public Opportunity Oppy;
	public ApexPages.StandardSetController stdCntrlr {get; set;}
	//constructor
	public OpportunityWrapper(Opportunity opp ,  ApexPages.StandardSetController controller)
	{
		Oppy = opp;
		stdCntrlr =controller ;
	}
	//compare opportunities based on the opportunity amount.
	global integer compareTo(object compareTo)
	{
		OpportunityWrapper compareToOppy = (OpportunityWrapper)compareTo;
		Integer returnvalue = 0;
		if(Oppy.Amount >	compareToOppy.Oppy.Amount)
		{
			returnvalue = 1;
		}else if(Oppy.Amount < compareToOppy.Oppy.Amount){
			returnvalue = -1;
		}
		return returnvalue;	
	}
}