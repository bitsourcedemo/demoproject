public with sharing class accountTestTriggerHandler
{
    public void accnttst(List<Account> lstaccount){
    
    Set<Id> setId = new Set<Id>();
    
    for(Account obj:lstaccount)
         {
             setId.add(obj.Id);
         }
        
    List<Account> accountsWithContacts = [select id, name, (select id, salutation, description, firstname, lastname, email from Contacts)
                                                                               from Account where Id IN :setId];
          List<Contact> contactsToUpdate = new List<Contact>{};
         // For loop to iterate through all the queried Account records
         for(Account  a: accountsWithContacts)
         {
             // Use the child relationships dot syntax to access the related Contacts
             for(Contact c: a.Contacts)
             {
                System.debug('Contact Id[' + c.Id + '], FirstName[' + c.firstname + '], LastName[' + c.lastname +']');
                
                c.Description = c.salutation + ' ' + c.firstName + ' ' + c.lastname;
                
                contactsToUpdate.add(c);
                System.Debug('value of contact list'+contactsToUpdate);
             }       
     }
    Update contactsToUpdate;
    System.Debug('value of contacts to update list '+contactsToUpdate);
  }
}