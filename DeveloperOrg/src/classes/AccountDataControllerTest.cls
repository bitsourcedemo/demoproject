@istest
public class AccountDataControllerTest
{
    public static testMethod void nextPageTest()
    {
        PageReference testPage = new pagereference('/apex/AccountDemo2');
        Test.setCurrentPage(testPage);
    }
    public static testMethod void PreviousPageTest()
    {
        PageReference testPage = new pagereference('/apex/AccountDemo');
        Test.setCurrentPage(testPage);
    }
}