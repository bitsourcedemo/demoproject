/*******************************************************************
 * Custom controller for the "Custom Lookup" recipe. Executes a 
 * search based on a query string entered by a user. 

 visualforce pages: lookup and lookupPopup
 *******************************************************************/
public with sharing class LookupController {
    
    // the query string entered by the user
    public String query{get;set;}

    // the matching Accounts
    public List<Account> accounts{get;set;}

    //search been executed?
    public boolean donelookup{get;set;}

    public LookupController() {
        donelookup = false;
    }

    //executes the search
    public pageReference runQuery()
    {
        //SOSL FOR SEARCHING THE STRING ENTERED BY THE USER.
        List<List<Account>> searchResults = [FIND :query IN ALL Fields RETURNING Account(Id, Name,BillingCity,Type,Rating,BillingStreet,BillingPostalCode)];
        accounts = searchResults[0];
        donelookup = true;
        return null;
    }
}