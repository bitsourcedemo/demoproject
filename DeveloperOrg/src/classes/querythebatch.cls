global with sharing class querythebatch {
	
	global void finish(Database.BatchableContext BC){
		AsyncApexJob a = [Select Id, Status,NumberOfErrors,JobItemsProcessed,TotalJobItems,CreatedBy.Email From AsyncApexJob Where Id =:BC.getJobId()];
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] {a.CreatedBy.Email};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Apex Sharing Recalculation ' + a.Status);
		mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}