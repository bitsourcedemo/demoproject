public class UpdateSalesRepHandler{
    set<Id> ownerIdSet = new Set<Id>();
    
    public void methodToUpdate(List<Account> accList){
        for(Account acc: accList){
            ownerIdSet.add(acc.OwnerId);
        } 
        Map<Id,User> UserMap = new Map<Id,User>([Select Name from User where id in : ownerIdSet]);
        for(Account accTemp:accList){
            User user = UserMap.get(accTemp.OwnerId);
                accTemp.Sales_Rep__c = user.Name;
        }
    }
}