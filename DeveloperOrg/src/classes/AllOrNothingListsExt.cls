/*******************************************************************
 * Extension controller for the "Custom Iterator Component" recipe. 
 * Manages the lists of contacts and accounts associated with the 
 * account managed by the standard controller.
 *******************************************************************/
public with sharing class AllOrNothingListsExt {
	//the Account contacts
	public List<Contact> contacts{get;set;}

	//the account opportunities
	public List<Opportunity> opportunities{get;set;}

	// constructor - retrieves the contacts and opportunities for the account
	// managed by the standard controller
	public AllOrNothingListsExt(ApexPages.StandardController stdCtrl) {
		Id accId = stdCtrl.getId();
		contacts = [Select Id,Salutation,FirstName,LastName,Title,Email From Contact Where Id =:accId];
		opportunities = [Select Id,Name,CloseDate,Amount,StageName,Probability From Opportunity Where Id =:accId];
	}
}