//web services example with the util classes.
// @author : Patrick 
public class AccountUtils {

    /**
    * Gets a map of account numbers to accounts
    * 
    * @param accountNumbers A set of account numbers
    * @return A map of account number to account
    */
    
    public static Map<String, Account> getAccountMapByNumber(Set<String> accountNumbers) 
    {
    
        Map<String, Account> results = new Map<String,Account>();
        
         for (Account account : [select AccountNumber,MyField__c,Name from Account where AccountNumber in :accountNumbers]) 
         {
            results.put(account.AccountNumber, account);
         }
             return results;
    }
    /**
    * Gets an account for a given account number
    *
    * @param acccountNumber The account number
    * @return The account
    * @throws GenericUtils.UnknownException if the account cannot be found
    */
    
    public static Account getAccountByNumber(String AccountNumber)
    {
    
        Map<String, Account> accountMap = getAccountMapByNumber(new Set<String>{accountNumber});
        
        if (!accountMap.containsKey(accountNumber))
        {
            throw new GenericUtils.UnknownException('Unable to find account');
        }
            return accountmap.get(accountNumber);
    }
    
    
}