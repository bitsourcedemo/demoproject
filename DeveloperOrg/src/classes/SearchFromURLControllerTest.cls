/*
    * This test class tests the functionality of the SearchFromURLController
*/
@isTest
private class SearchFromURLControllerTest {
    
    //test searching when the search term appears on the URL.
    static testMethod void searchFromURLTest(){
         
        Test.StartTest();
        List<Account> accs=new List<Account>();
        accs.add(new Account(Name='Unit Test',Industry = 'Energy',Type ='Other',Rating = 'Hot'));
        accs.add(new Account(Name='Unit Test 2',Industry = 'Energy',Type ='Other',Rating = 'Hot'));
        accs.add(new Account(Name='The Test Account'));
        insert accs;
        
        PageReference pr=Page.SearchFromURL;
        pr.getParameters().put('name', 'Unit');
        pr.getParameters().put('industry', 'Energy');
        pr.getParameters().put('type', 'Hot');
         
        Test.setCurrentPage(pr);
        
        SearchFromURLControllerOriginal controller=new SearchFromURLControllerOriginal();
        System.assertEquals(true, controller.searched);
        System.assertEquals(2, controller.accounts.size());
        Test.StopTest();   
    }
   
    //test searching when users enters a value.
    static testMethod void executeSearchTest()
    {
           Test.StartTest();
           List<Account> acc = new List<Account>();
           acc.add(New Account(Name ='Bernard',Industry = 'Energy',Type ='Other',Rating = 'Hot'));
           
           insert acc;
           system.debug('value of the list in the executeSearchTest function++++++++++++'+acc);
           
           //pagereference instantiation
           PageReference pr=Page.SearchFromURL;
           pr.getParameters().put('name','Bernard');
           pr.getParameters().put('industry','Energy');
           pr.getParameters().put('rating','hot');
           pr.getParameters().put('type','other');
           system.debug('value of the list in the executeSearchTest function++++++++++++'+pr);
           Test.setCurrentPage(pr);
           SearchFromURLController controller=new SearchFromURLController();
           system.assertEquals(false,controller.searched);
           //System.assertEquals(1, controller.performSearch().size());
           Test.StopTest();
    }
}