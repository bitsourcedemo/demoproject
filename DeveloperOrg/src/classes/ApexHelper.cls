global without sharing  class ApexHelper{
    
    // ==============================================================================
    // Page Methods  - Declaring Methods in HelperClass can be used repetedly, promotes 
    // code re-usage
    // ==============================================================================
      public static string GetValueFromParam(string ParamKey)
    {
        string toReturn='';
        if(ApexPages.currentPage().getParameters().containsKey(ParamKey))
        {            
            toReturn=ApexPages.currentPage().getParameters().get(ParamKey);
            //toReturn=ApexUtil.GetCleanString(toReturn);        
        }
        return toReturn;
    }      
  
}