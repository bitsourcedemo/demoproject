public with sharing class OutBoundEmailExample {
        
        public string la{get;set;}
        public void methodOfEmail(){
                
                // regulating the limit of sending the email so that it would not exceed the limit of the organization
                Messaging.reserveSingleEmailCapacity(2);
                
                //now creating the single email message object
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailmessage();
                
                
                // now creating the strings to hold the address in which we are sending the email
                String[]  toAddresses = new String[]{'lalitsngh@gmail.com'};
                String[]  ccAddresses = new String[]{'lalitsngh@yahoo.com'};
                
                // assigning the above addresses to the cc and to adresses
                mail.setToAddresses(toAddresses);
                mail.setCcAddresses(ccAddresses);
                
                // specify the address used when the receipients reply to the email
                mail.setReplyTo('lalit.singh@accenture.com');
                
                // Specify the name used as the display name.
                mail.setSenderDisplayName('Salesforce Support');
                
                // Specify the subject line for your email address.
                mail.setSubject('New Case Created : ' + case.Id);
                
                // Set to True if you want to BCC yourself on the email.
                mail.setBccSender(false);
                
                mail.setUseSignature(false);
                
                // Specify the text content of the email.
                
                mail.setPlainTextBody('Your Case: ' + case.Id +' has been created.');
                
                mail.setHtmlBody('Your case:<b> ' + case.Id +' </b>has been created.<p>'+'To view your case <a href=https://ap1.salesforce.com/'+case.Id+'>click here.</a>');
                
                
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }

}