global class BatchDelete implements Database.Batchable<sObject>
{
    public String query;
    global Database.queryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {
        delete scope;
        Database.emptyRecycleBin(scope);
    }
    global void finish(Database.BatchableContext BC)
    {
    }
}