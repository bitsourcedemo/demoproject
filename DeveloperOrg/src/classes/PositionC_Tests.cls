@isTest private class PositionC_Tests{
    testmethod private static void testAssignRecruiterRegion1()
    {
        User testuser =  TestFactory.buildTestUser(0,'Custom-Recruiter');
        insert testuser;
        Position__c testposition = TestFactory.buildTestposition(0,testuser.Id);
        insert testposition ;
        Position__c dbPosition = [Select Region__c from Position__c Where Id =:testposition.Id];
        System.assertEquals(dbPosition.Region__c,testUser.Region__c);
    }


}