/*******************************************************************
 * Custom controller for serach functionality.
 * author : Bala & Lalit
    page name : SearchFromURL
 *******************************************************************/
public with sharing class SearchFromURLController 
{
    // Variable Declaration
    public String topass{get;set;}
    
    public String value{get;set;}
    
    public List<Account> accounts {get; set;}
    
    public Boolean searched {get; set;}
    
    
    
    //Constructor Decalaration
    public SearchFromURLController()
    {
        searched=false;
        String nameStr=ApexPages.currentPage().getParameters().get('value');
         if (null!=nameStr)
        {
            value=nameStr;
            executeSearch();
        }
    }
    // action method to call the runsearch method.
    public pageReference executeSearch()
    {
        searched=true;
        accounts = performSearch(value);
        return null;
    }
    
    //action method to return the values to pageblockTable.
    private List<Account> performSearch (String value)
    {
        topass ='select id, Name, Industry, Type, Rating from Account where';  
        topass = topass+'(name LIKE \'%'+ value +'%\'OR industry LIKE \'%'+ value +'%\'OR rating LIKE \'%'+ value + '%\'OR type LIKE \'%'+ value + '%\')';
        return Database.query(topass);
    }
     public pageReference edit(){
        PageReference pg = page.EditFromSearch;
        Integer idx =1;
        for(Account acc :Database.query(topass)){
            pg.getParameters().put('account' + idx, acc.id);
            idx++;
        }
        return pg;
    }
}